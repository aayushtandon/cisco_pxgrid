__author__ = 'Aujas'

from qpylib import qpylib
import json
from time import sleep
import requests
class QRadarAPIClient:
    def __init__(self, version=None, username=None, password=None):
        self.SEC = None
        self.QRadarCSRF = None
        #how many number of times the system should attempt to fetch results
        self.threshold=1000
        self.endpoint_start = 'api/ariel/searches'

        self.version= "7.0"
        if version is not None:
            self.version=version
        self.qradar_api_instance = qpylib.strategy()
        qradar_ip = self.qradar_api_instance.get_console_address()
        self.headers = {"Version":version,'mime-type': 'application/json', 'Accept': 'application/json','Keep-Alive': 'timeout=5, max=50','Authorization': 'Basic base64Encoding'}
        qpylib.log("IP address " + qradar_ip)
        #nothing much to do but we just need to ensure to check for auth and other initial setup.


    def getBaseURL(self):
        return 'https://' + self.qradar_api_instance.get_console_address() + '/'


    def getHeaders(self):
        return {"Version":self.version,'mime-type': 'application/json', 'Accept': 'application/json', 'Keep-Alive': 'timeout=30, max=50'}


    #method to create a search
    def create_search(self, query_expression,user,pwd):
        endpoint= self.endpoint_start
        data = {'query_expression': query_expression}
        ip  = self.qradar_api_instance.get_console_address()
        url = 'https://' + ip + '/'+endpoint
        response = requests.post(url, data=data, auth=(user, pwd), verify=False, headers=self.headers)
        return response


    def get_search(self, search_id,user, pwd, csrf = None, sec = None):
        endpoint = self.endpoint_start + "/" + search_id
        ipaddress = self.qradar_api_instance.get_app_base_url()

        ip = self.qradar_api_instance.get_console_address()
        url = 'https://'+ip+'/'+endpoint
        headers = self.headers.copy()
        response = requests.get(url, auth=(user, pwd), verify=False, headers=headers)
        return response

    def get_search_results(self, search_id, response_type,user,pwd,range_start=None, range_end=None):

        headers = self.headers.copy()
        headers['Accept'] = response_type
        if ((range_start is not None) and (range_end is not None)):
            headers[b'Range'] = ('items=' + str(range_start) + '-' + str(range_end))
        ip=self.qradar_api_instance.get_console_address()
        endpoint = 'https://'+ip+'/'+ self.endpoint_start + '/' + search_id + '/results'
        response = requests.get(endpoint, auth=(user,pwd), verify=False, headers=headers)

        return response

    def update_search(self, search_id, save_results=None, status=None):

        # sends a POST request to
        # https://<server_ip>/rest/api/ariel/searches/<search_id>
        # posts search result to site
        endpoint = self.endpoint_start + "/" + search_id

        data = {}
        if save_results:
            data['save_results'] = save_results
        if status:
            data['status'] = status
        return self.qradar_api_instance.REST('POST',endpoint, self.headers, data=data)


    def delete_search(self, search_id,user,pwd):
        headers = self.headers.copy()
        # sends a DELETE request to
        # https://<server_ip>/rest/api/ariel/searches/<search_id>
        # deletes search created earlier.
        endpoint = self.endpoint_start + "/" + search_id        
        ip = self.qradar_api_instance.get_console_address()
        endpoint = 'https://' + ip + '/' + self.endpoint_start + '/' + search_id
        response = requests.delete(endpoint, auth=(user, pwd), verify=False, headers=headers)
        return response


    def wait_get_search_results(self, query_expression, pagenumber, pagesize,user,pwd):
        data = {}
        try:
            response = self.create_search(query_expression,user,pwd)
            sleep(2)  # sleep for 1s so we have
            response_json = json.loads(response.text)
            search_id = response_json['search_id']

            sleep(1) #sleep for 1s so we have
            response = self.get_search(search_id,user,pwd)
            response_json = json.loads(response.text)
            error = False
            counter = 0
            while (response_json['status'] != 'COMPLETED') and not error:
                if ((response_json['status'] == 'EXECUTE') | \
                        (response_json['status'] == 'SORTING') | \
                        (response_json['status'] == 'WAIT') ) & \
                        (counter < self.threshold) :
                        qpylib.log("Final  " + str(response_json['record_count']))
                        qpylib.log("Processed " + str(response_json['processed_record_count']))
                        sleep(0.3) #sleep for 300 ms so some one else can get the CPU
                        response = self.get_search(search_id,user,pwd)
                        response_json = json.loads(response.text)
                        counter = counter +1
                else:
                    error = True
                    qpylib.log("Cancel the query " + search_id)
                    self.delete_search(search_id,user,pwd)

            data['total_records'] = response_json['record_count']
            pagenumber= 0 if pagenumber == None else pagenumber
            pagesize= 0 if pagesize == None else pagesize
            page_range = self.convert_page_to_range(pagenumber,pagesize)
            response = self.get_search_results(search_id, 'application/json',user,pwd, page_range['start'], page_range['end'])
            qpylib.log("Final range start "+ str(page_range['start']) + ' end ' + str(page_range['end']))
            body = response.text
            body_json = json.loads(body)
            data['response_json'] = body_json
            #self.delete_search(search_id)
        except:
            qpylib.log("Error in getting search results : method -> wait_get_search_results")
            qpylib.log(query_expression)
        return data


    def prepare_export_search_results(self, query_expression):
        response = self.create_search(query_expression)
        response_json = json.loads(response.text)
        search_id = response_json['search_id']
        response = self.get_search(search_id)
        error = False
        while (response_json['status'] != 'COMPLETED') and not error:
            if (response_json['status'] == 'EXECUTE') | \
                    (response_json['status'] == 'SORTING') | \
                    (response_json['status'] == 'WAIT'):
                response = self.get_search(search_id)
                response_json = json.loads(response.text)
            else:
                error = True
        data={}
        data['total_records'] = response_json['record_count']
        data['search_id']=search_id
        return data


    def get_completed_search_results(self,search_id,range_start,range_end):
        response = self.get_search_results(search_id, 'application/json', range_start, range_end)
        body = response.text
        body_json = json.loads(body)
        return body_json


    def export_completed_search_results(self,search_id,range_start,range_end):
        response = self.get_search_results(search_id, 'application/csv', range_start, range_end)
        return response.text


    def convert_page_to_range(self,pagenumber,pagesize):
        if int(pagenumber) < 0:
            pagenumber=0
        if int(pagesize) <= 1:
            pagesize=2
        page_range={'start':(int(pagenumber))*int(pagesize),
            'end': ((int(pagenumber))*int(pagesize)) + (int(pagesize)-1)
        }
        return page_range
