__author__ = 'Aujas'

from string import Template
from datetime import datetime
from qpylib import qpylib

class QueryHelper:
    #def __init__(self):

    #returns the final executable query after template management.
    #TODO: We should check the tokds for any possible SQL injection.
    #But not sure if this matters in the AQL/QRadar world, as the users can anyway run query. May be check with IBM.
    def get_query_expression(self,query_template,tokens):
        s=Template(query_template)
        #TODO:Check for sql injection here
        query_expression = s.substitute(tokens)
        return query_expression

    #Let us fix the date time format dynamically
    def get_date_format(self,start_date,end_date):
        #date_format = "%m/%d/%Y"
        delta = end_date - start_date
        if delta.days > 7 :
            return "YYYY-MM-d"
        if delta.days > 1 :
            return "YYYY-MM-d HH"
        return "YYYY-MM-d HH:mm"

    #Let us fix the date time format dynamically for sqlite3
    def get_sqllite_date_format(self,start_date,end_date):
        #date_format = "%m/%d/%Y"
        delta = end_date - start_date
        #qpylib.log("Total days " + str(delta.days))
        if delta.days > 7 :
            return "%H"
        #if delta.days > 1 :
        #    return "%s"
        return "%M"

    #This a common crossfilter option across the DLP and ATP dashboards
    def query_filter_creator(self,params):
        query_filter = ''
        #Check for Machine and accept multiple machine names
        if 'machine' in params and params['machine'] != '':
            if(params['machine'] > 0):
                query_filter= ' (' if query_filter=='' else query_filter + ' AND ('
            first= True
            for machine in params['machine']:
                #qpylib.log("Yes we got this " + machine)
                query_filter= query_filter + ' Resource= \'' + machine + '\'' if first else query_filter + ' OR Resource= \'' + machine + '\''
                first= False
            if(params['machine'] > 0):
                query_filter= query_filter + ' )'
        #Check for user and accept multiple user names
        if 'user' in params and params['user'] != '':
            if(params['user'] > 0):
                query_filter= ' (' if query_filter=='' else query_filter + ' AND ('
            first= True
            for user in params['user']:
                query_filter= query_filter + ' username= \'' + user + '\'' if first else query_filter + ' OR username= \'' + user + '\''
                first=False
            if(params['user'] > 0):
                query_filter= query_filter + ' )'
        #Check for process and accept multiple process names    - Note process is used only in ATP
        if 'process' in params and params['process'] != '':
            if(params['process'] > 0):
                query_filter= ' (' if query_filter=='' else query_filter + ' AND ('
            first= True
            for process in params['process']:
                query_filter= query_filter + ' application= \'' + process + '\'' if first else query_filter + ' OR application= \'' + process + '\''
                first=False
            if(params['process'] > 0):
                query_filter= query_filter + ' )'
        #Check for file and accept multiple file names
        if 'file' in params and params['file'] != '':
            if(params['file'] > 0):
                query_filter= ' (' if query_filter=='' else query_filter + ' AND ('
            first= True
            for ifile in params['file']:
                query_filter= query_filter + ' Filename= \'' + ifile + '\'' if first else query_filter + ' OR Filename= \'' + ifile + '\''
                first=False
            if(params['file'] > 0):
                query_filter= query_filter + ' )'
        #Check for rule and accept multiple rule names
        if 'rule' in params and params['rule'] != '':
            if(params['rule'] > 0):
                query_filter= ' (' if query_filter=='' else query_filter + ' AND ('
            first= True
            for rule in params['rule']:
                query_filter= query_filter + ' "Rule Name" = \'' + rule + '\'' if first else query_filter + ' OR "Rule Name" = \'' + rule + '\''
                first=False
            if(params['rule'] > 0):
                query_filter= query_filter + ' )'


        if query_filter == '':
            qpylib.log("No machine/user/process filter requested")
            return ""
        query_filter='AND (' + query_filter + ')'
        return query_filter
