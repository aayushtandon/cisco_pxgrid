'use strict';

$(function() {
  fetchData();
});

function capitalizeTxt(txt) {
  return txt.charAt(0).toUpperCase() + txt.slice(1);
}
function fetchData() {
  return $.ajax({
    method: 'get',
    url: 'cisco/api/v1/system/settings/list',
    beforeSend: function (xhr){
      xhr.setRequestHeader("Accept","application/json");
      return xhr.setRequestHeader('X-CSRFToken', $("#csrf_token").val());
    },
    success: function success(data) {
      var settings = formatJSON(JSON.parse(data));
      populateForm(settings);
    }
  });
}

function formatJSON(data) {
  var settings = {};
  for (var index = 0; index < data.length; index++) {
    var section = data[index].section;
    if (!(section in settings)) {
      settings[section] = [];
    }
    settings[section].push(data[index]);
  }

  Object.keys(settings).forEach(function(section) {
    settings[section].sort(function(current, next) {
      return current.display_order > next.display_order;
    });
  });

  return settings;
}

function populateForm(settings) {
    var button = '<br><button class="btn btn-warning float-lg-right" type="submit" style="cursor:pointer;" id="testButton">Submit and Test Connection</button><button class="btn btn-success float-lg-right" type="submit" id="submitButton" style="display:none;">Submit</button><br><br><br>';

    var formData = '';

    var tabOutlet = '<ul class="nav nav-tabs" role="tablist">';
    Object.keys(settings).forEach(function(group, index) {
      var active = '';
      if(index == 0){
        active = 'active';
      }
      tabOutlet = tabOutlet+'<li class="nav-item"><a data-toggle="tab" role="tab" class="nav-link '+active+'" href="#'+group+'">'+capitalizeTxt(group)+'</a></li>';
    });
    tabOutlet = tabOutlet+'</ul>';
    formData = formData+tabOutlet;

    var tabContentOutlet = '<div class="tab-content">';
    Object.keys(settings).forEach(function(group, index) {
      var active = '';
      if(index == 0){
        active = ' in active';
      }
      var $container = '<div class="tab-pane fade'+active+'" id="'+group+'" role="tabpanel"><h3></h3>';
      settings[group].forEach(function(entry) {
        var required = "";
        if(group == "primary"){
           required = "required";
        }
        $container = $container+convertToElement(entry, required);
      });
      tabContentOutlet = tabContentOutlet+$container+'</div>';
    });
    tabContentOutlet = tabContentOutlet+'</div>';
    formData = formData+tabContentOutlet+button;

    $('#app-settings').html(formData);
    listenToFormSubmit();
}

function handleDropDown(entry) {
  var $container = $('<div />', { class: 'form-group row' }).append($('<label class="col-sm-4 col-form-label"/>').html(entry.key_label));
  var $selectContainer = $('<div />', { class: 'col-sm-8' });
  var $select = $('<select/>', { class: 'form-control', 'data-label': '' + entry.key_label, 'data-key': '' + entry.key, 'data-id': '' + entry.id });
  if (entry.data) {
    entry.data.split(',').map(function (option) {
      var $optionNode = $('<option/>', { value: '' + option }).text(option);
      $select.append($optionNode);
    });
  }
  $selectContainer.append($select);
  $container.append($selectContainer);

  return $container;
}

function handleBoolean(entry) {
  var element = '<div class="form-group row">' + ('<label class="col-sm-4">' + entry.key_label + '</label>') + '<div class="radio-inline">' + '<label>' + ('<input type="radio" name="' + entry.key_label + '" data-label="' + entry.key_label + '" data-key="' + entry.key + '" data-id="' + entry.id + '" value="true"') + (' ' + (entry.value == 'true' ? "checked" : "") + ' ') + '><b>Yes</b>' + '</label>' + '</div>' + '<div class="radio-inline">' + '<label>' + ('<input type="radio" name="' + entry.key_label + '" data-label="' + entry.key_label + '" data-key="' + entry.key + '" data-id="' + entry.id + '" value="false"') + (' ' + (entry.value == 'false' ? "checked" : "") + ' ') + '><b>No</b>' + '</label>' + '</div>' + '</div>';
  return element;
}

function handleCheckbox(entry) {
  var $container = $('<div />', { class: 'form-group row' }).append($('<label class="col-sm-4 col-form-label"/>').html(entry.key_label));
  var el = $('<div/>', { class: 'col-sm-8' });
  if (entry.data) {
    (function () {
      var checkedArray = entry.value.split(',');
      entry.data.split(',').map(function (choice) {
        var wrapper = $('<div/>', { class: 'checkbox' });
        var label = $('<label class="col-sm-4 col-form-label"/>');
        var checkbox = $('<input/>', { type: 'checkbox',
          name: entry.key,
          'data-label': entry.key_label,
          'data-key': entry.key,
          'data-id': entry.id,
          value: choice
        }).prop('checked', !!($.inArray(choice, checkedArray) > -1)).add($('<span/>').html(choice));
        label = label.append(checkbox);
        wrapper = wrapper.append(label);
        el = el.append(wrapper);
      });
      $container.append(el);
    })();
  }
  return $container;
}

function handleInteger(entry, required) {
  var asterisk = '';
  if(required == "required"){
    asterisk = '&nbsp;&nbsp;<i class="mandatoryIcon">*</i>';
  }
  var element = '<div class="form-group row"><label class="col-sm-4 col-form-label">' + entry.key_label+asterisk + '</label>' + '<div class="col-sm-8">' + ('<input type="number" class="form-control" data-label="' + entry.key_label + '" data-key="' + entry.key + '" data-id="' + entry.id + '" value="' + entry.value + '" '+required+'>') + '</div></div>';
  return element;
}

function handleTextAndPassword(entry, required) {
  var asterisk = '';
  if(required == "required"){
    asterisk = '&nbsp;&nbsp;<i class="mandatoryIcon">*</i>';
  }
  var element = '<div class="form-group row"><label class="col-sm-4 col-form-label">' + entry.key_label +asterisk+ '</label>' + '<div class="col-sm-8">' + ('<input type=' + entry.object_type + ' class="form-control" data-label="' + entry.key_label + '" data-key="' + entry.key + '" data-id="' + entry.id + '" value="' + entry.value + '"  '+required+'>') + '</div></div>';
  return element;
}

function handleTextArea(entry, required) {
  var asterisk = '';
  if(required == "required"){
    asterisk = '&nbsp;&nbsp;<i class="mandatoryIcon">*</i>';
  }
  var element = '<div class="form-group row"><label class="col-sm-4 col-form-label">' + entry.key_label+asterisk + '</label>' + '<div class="col-sm-8">' + ('<textarea rows="3" class="form-control" data-label="' + entry.key_label + '" data-key="' + entry.key + '" data-id="' + entry.id + '" value="' + entry.value + '" '+required+'></textarea>') + '</div></div>';
  return element;
}

function handleFile(entry, required) {
  var asterisk = '';
  if(required == "required"){
    asterisk = '&nbsp;&nbsp;<i class="mandatoryIcon">*</i>';
  }
  var element = '<div class="form-group row"><label class="col-sm-4 col-form-label">' + entry.key_label+asterisk + '</label>' + '<div class="col-sm-8">' + ('<input type="File" name="file" class="form-control" accept=".cer,.key,.zip" data-label="' + entry.key_label + '" data-key="' + entry.key + '" id="' + entry.section + '" multiple '+required+'>') + '</div></div>';
  return element;
}

function convertToElement(entry, required) {
  switch (entry.object_type) {
    case 'boolean':
      return handleBoolean(entry, required);
    case 'checkbox':
      return handleCheckbox(entry, required);
    case 'dropdown':
      return handleDropDown(entry, required);
    case 'integer':
      return handleInteger(entry, required);
    case 'password':
    case 'text':
      return handleTextAndPassword(entry, required);
	case 'textarea':
      return handleTextArea(entry, required);
	case 'file':
      return handleFile(entry, required);
    default:
      console.log(entry.object_type);
  }
}

function listenToFormSubmit() {
  $('#app-settings button').on('click', function(e) {
    onAppSettingSubmit(e);
  })
}

function pushSettingsValue($element, value, settings) {
  settings.push({
    key_label: $element.data('label'),
    value: value,
    key: $element.data('key'),
    id: $element.data('id')
  });
}

function populateTextNumberPasswordAndSelectValues($elements, settings) {
  $elements.each(function() {
    pushSettingsValue($(this), $(this).val(), settings);
  });
}

function populateRadioValues($elements, settings) {
  var dataKeys = [];

  $elements.each(function() {
    var dataKey = $(this).data('key');
    if (dataKeys.indexOf(dataKey) == -1) dataKeys.push(dataKey);
  });

  dataKeys.forEach(function(dataKey) {
    $('[data-key="' + dataKey + '"]').each(function() {
      var $element = $(this);
      if($element.is(':checked')) pushSettingsValue($element, $element.val(), settings);
    });
  });
}

function populateCheckboxValues($elements, settings) {
  var dataKeys = [];

  $elements.each(function() {
    var dataKey = $(this).data('key');
    if (dataKeys.indexOf(dataKey) == -1) dataKeys.push(dataKey);
  });

  dataKeys.forEach(function(dataKey) {
    var $checkboxes = $('[data-key="' + dataKey + '"]');
    var value = '';

    $checkboxes.each(function() {
      var $element = $(this);
      if($element.is(':checked')) {
        if (value.length == 0) return value =  $element.val();

        value = value + ',' + $element.val();
      }
    });

    pushSettingsValue($($checkboxes[0]), value, settings);
  });
}

function onAppSettingSubmit(e) {
  e.preventDefault();
  if($("#app-settings").valid()){
    var $form = $('#app-settings');
    var settings = [];


    populateTextNumberPasswordAndSelectValues($form.find('select,input[type="text"],input[type="password"],input[type="number"]'), settings);
    populateRadioValues($form.find('input[type="radio"]'), settings);
    populateCheckboxValues($form.find('input[type="checkbox"]'), settings);

    postDataFile(settings);
  }
}

function postDataFile(settings) {
  $('button').prop('disabled', true);
  $.each($("input[type='file']"), function(i, dt) {
    var baseDir = $(this).attr("id");
    $.each($("input#"+baseDir)[0].files, function(j, file) {
      var data = new FormData();
      data.append('file', file);
      $.ajax({
        url: 'cisco/api/v1/system/settings/update/'+baseDir+'/files',
        method: 'POST',
        data: data,
        contentType: false,
        processData: false,
        beforeSend: function (xhr){
      xhr.setRequestHeader("Accept","application/json");
      return xhr.setRequestHeader('X-CSRFToken', $("#csrf_token").val());
    },
      }).done(function () {

      }).fail(function () {
          //alert('Failed to update application settings');
      });
    });
    if(parseInt($("input[type='file']").length) == parseInt(parseInt(i)+1)){
      postData(settings)
    }
  });
}

function postData(settings) {
  return $.ajax({
    url: 'cisco/api/v1/system/settings/update',
    method: 'POST',
    data: JSON.stringify(settings),
    beforeSend: function (xhr){
      xhr.setRequestHeader("Accept","application/json");
      return xhr.setRequestHeader('X-CSRFToken', $("#csrf_token").val());
    },
    contentType: 'application/json'
  }).done(function (data, statusText, xhr) {
    alert('Successfully updated application settings');
    testConnections();
  }).fail(function () {
    alert('Failed to update application settings');
  });
}

function testConnections(){
  $.each($("input[type='file']"), function(i, dt) {
    var baseDir = $(this).attr("id");
    $.each($("input#"+baseDir)[0].files, function(j, file) {
      if(parseInt($("input#"+baseDir)[0].files.length) == parseInt(parseInt(j)+1)){
        $.ajax({
          url: 'cisco/api/v1/system/settings/test/'+baseDir+'/files',
          method: 'POST',
          contentType: false,
          processData: false,
          beforeSend: function (xhr){
              xhr.setRequestHeader("Accept","application/json");
              return xhr.setRequestHeader('X-CSRFToken', $("#csrf_token").val());
            },
        }).done(function (data, statusText, xhr) {
              if(data === '200'){
                alert(baseDir+' server connection successful');
              }else{
                alert(baseDir+' server connection failed');
              }

        }).fail(function () {
           alert(baseDir+' server connection failed');
        });
      }
    });
  });
  $('button').prop('disabled', false);
}
