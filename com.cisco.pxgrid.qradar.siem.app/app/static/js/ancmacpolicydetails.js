(function($) {
  $.ajax({
    url: $("#baseurl").text()+"/../cisco/api/v1/getEndpoints",
    type:'GET',
    contentType: "application/json; charset=utf-8",
    cache: false,
    beforeSend: function (xhr){
      xhr.setRequestHeader("Accept","application/json");
    },
    error: function(xhr) {
      console.log("response.statusMessage>>>"+JSON.parse(xhr.responseText).statusMessage);
    }
  }).then(function(policyLists) {
    if(policyList != "null"){
      var policyList = [];
      $.each(JSON.parse(policyLists).endpoints, function( index, value ) {
        var policyName = {};
        policyName["Mac Address"] = value.macAddress;
        policyName["Policy Name"] = value.policyName;
        policyList.push(policyName);
      });
      $('#ancPolicyDisplay').DataTable({
        "pageLength": 10, 
        "processing": true,
        "serverSide": false,
        "searching": false,
        data: policyList,
        columns: [
          { "title" : "Mac Address", "data" : "Mac Address" },
          { "title" : "Policy Name", "data" : "Policy Name" }
        ]
      });
    }
  });
})($); // End of use strict
