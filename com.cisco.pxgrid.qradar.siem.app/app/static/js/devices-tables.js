// Call the dataTables jQuery plugin
$(document).ready(function() {
	if($.fn.DataTable.isDataTable('#devicedataTable') ) {
		$('#devicedataTable').DataTable().destroy();
	}
    $('#devicedataTable').DataTable({
        "scrollX": true
    });
});
