var ctx = document.getElementById("grouptag");
var grouptag = new Chart(ctx, {
	type: 'bar',
	data: {
		labels: ["Employee", "Endpoint", "Manager", "Network"],
		datasets: [{
			backgroundColor: ["#3cba9f","#e8c3b9"],
			data: [13, 5],
		}],
	},
	options: {
		scales: {
			xAxes: [{
				time: {
					unit: 'count'
				},
				gridLines: {
					display: true
				},
				ticks: {
					maxTicksLimit: 6,
					beginAtZero: true
				}
			}],
			yAxes: [{
				ticks: {
					min: 0,
					max: 18,
					maxTicksLimit: 5
				},
				gridLines: {
					display: true
				}
			}],
		},
		legend: {
			display: false,
			position: 'bottom',
			labels: {
				boxWidth:8
			}
		},
		animation: {
			duration: 0,
			onComplete: function () {
				var ctx = this.chart.ctx;
				ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, 'normal', Chart.defaults.global.defaultFontFamily);
				ctx.fillStyle = this.chart.config.options.defaultFontColor;
				ctx.textAlign = 'center';
				ctx.textBaseline = 'bottom';
				this.data.datasets.forEach(function (dataset) {
					for (var i = 0; i < dataset.data.length; i++) {
						var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
						ctx.fillText(dataset.data[i], model.x, model.y - 5);
					}
				});
			}
		}
	}
});