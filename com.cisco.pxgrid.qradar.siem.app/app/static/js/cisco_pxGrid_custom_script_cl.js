var httprequest = null;
var destipaddress = null;
$( document ).ready( function() {
    domapi.addEvent( defaultTable, "selectionchanged", cl_anc_action_enable);
} );

$( window ).bind( "toolbarLoaded", cl_anc_action_enable );
function cl_anc_action_enable() {

    var isOK = ( pageId === "EventList" ) || (pageId === "OffenseList") || ( selectedRows.length == 1 );
    setActionEnabled( "anc_clear", isOK );
}


function getIPAddress(selectedRow) {
    ipaddress = null;
    columns = selectedRow.getElementsByTagName('td');
    for ( var i = 0;  i < columns.length; i++) {
        htmltxt = columns[i].outerHTML.toString();
        if(htmltxt.toString().indexOf("destinationIP") == -1) {
            continue;
        }else{
          spantag = columns[i].getElementsByTagName('span');
            if (spantag != null && spantag.length == 2) {
				      ipaddress = spantag[1].innerText;
			      }
		    }
    }
  return ipaddress;
}


function anc_clear(result) {
  
    if ( pageId === "EventList" || pageId === "OffenseList") {

      if (selectedRows.length === 0) {
            alert("Please select an event.");
            return;
        }
        else if (selectedRows.length > 1) {
            alert("Please select a single event.");
            return;
        } 
    }

    
    jsonObj = JSON.parse(JSON.stringify(result));
    localStorage.setItem("csrf_token", jsonObj.csrf_token);
    if (jsonObj.applicationId != null && parseInt(jsonObj.applicationId) > 0) {
      
      url = "plugins/" + jsonObj.applicationId + "/app_proxy/cisco/api/v1/anc_clear";
      response = getResposneFromAPI(url, jsonObj.context,'ip');

    } else {
      alert("Selected offence has multiple IP address involved and unable to fulfill the request");
    }

}

var handleResponse = function (status, responseTxt) {
  if (status == 200) {
      var jsonObj = JSON.parse(responseTxt);
      if (jsonObj.status === "FAILURE") {
        alert("Status: "+ jsonObj.status + "\r\nReason for failure : "+ jsonObj.failureReason);
      } else if (jsonObj.status === "SUCCESS") {
        alert("Successfully cleared the pxGrid ANC");
      } else{
        alert("ANC action returned status: "+ jsonObj.status);
      }
   } else {
        alert("Failed to invoke ANC API :"+ status);
   }
}


function getResposneFromAPI(api_url,context_val,cxt_type) {
    try {
    // Opera 8.0+, Firefox, Chrome, Safari
      if (window.XMLHttpRequest) {        
       httprequest = new XMLHttpRequest();
      } else if (window.ActiveXObject) {
       httprequest = new ActiveXObject("Microsoft.XMLHTTP");
      } else {
       httprequest = new ActiveXObject("Msxml2.XMLHTTP");
      }
    } catch (ex) {
    // Browser not supported
    console.log("This browser is not supported this functionality. Please try with another browser");
    }
   httprequest.onreadystatechange = processRequest;
   httprequest.open("POST", api_url, true);
   httprequest.setRequestHeader('Access-Control-Allow-Methods', 'POST');
   httprequest.setRequestHeader('Content-Type', 'application/json; charset=utf-8');
   httprequest.setRequestHeader('Access-Control-Allow-Headers', 'XMLHttpRequest,Content-Type, Authorization, X-Requested-With','Origin','Access-Control-Allow-Origin');
   httprequest.setRequestHeader("X-CSRFToken", localStorage.getItem("csrf_token"));
   httprequest.send(JSON.stringify({context:context_val,type:cxt_type}));
}


function processRequest() {
  switch (httprequest.readyState) {
    case 0 : // UNINITIALIZED
    case 1 : // LOADING
    case 2 : // LOADED
    case 3 : // INTERACTIVE
    break;
    case 4 : // COMPLETED
        //alert(httprequest.responseText);
        //alert(httprequest.status);
        handleResponse(httprequest.status, httprequest.responseText);
    break;
    default: alert("error");
 }
}
