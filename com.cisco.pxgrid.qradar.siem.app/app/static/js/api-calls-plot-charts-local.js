
function getsearchId(chartDetails) {
  if(chartDetails.chartType == "pie"){
    $("#"+chartDetails.title).html('<span style="font:18px verdana;margin-left:20px;">Loading...</span>');     
  }else if(chartDetails.chartType == "bar"){
    document.getElementById(chartDetails.title).getContext("2d").font = '18px verdana';
    document.getElementById(chartDetails.title).getContext("2d").fillText('Loading...',10,40);
  }  
  var beforeSevenDays = $('#IDOfDateRangePicker').data('daterangepicker').startDate.format('YYYY-MM-DD HH:mm');
  var currentDate = $('#IDOfDateRangePicker').data('daterangepicker').endDate.format('YYYY-MM-DD HH:mm');
  $.ajax({   
    url: "../../"+chartDetails.appName+"/api/v1/range_search/call_to_all",
    type:'POST', 
    data: JSON.stringify({"query_param":{"start_date" : beforeSevenDays, "end_date" : currentDate, "query_name" : chartDetails.url}}),
    contentType: "application/json; charset=utf-8",
    cache: false,
    beforeSend: function (xhr){ 
      xhr.setRequestHeader("Accept","application/json");
       return xhr.setRequestHeader('X-CSRFToken', $("#csrf_token").val());
    },
    error: function(xhr) {
      console.log("response.statusMessage>>>"+JSON.parse(xhr.responseText).statusMessage);
    }
  }).then(function(searchIdResponse) {
    getstatus(chartDetails, searchIdResponse.search_id);
  });
}
function getSearchMDMDetails(chartDetails) {
  if(chartDetails.chartType == "pie"){
    $("#"+chartDetails.title).html('<span style="font:18px verdana;margin-left:20px;">Loading...</span>');
  }else if(chartDetails.chartType == "bar"){
    document.getElementById(chartDetails.title).getContext("2d").font = '18px verdana';
    document.getElementById(chartDetails.title).getContext("2d").fillText('Loading...',10,40);
  }
  var beforeSevenDays = $('#IDOfDateRangePicker').data('daterangepicker').startDate.format('YYYY-MM-DD HH:mm');
  var currentDate = $('#IDOfDateRangePicker').data('daterangepicker').endDate.format('YYYY-MM-DD HH:mm');
  $.ajax({
    url: "../../"+chartDetails.appName+"/api/v1/"+chartDetails.url,
    type:'GET',
    contentType: "application/json; charset=utf-8",
    cache: false,
    beforeSend: function (xhr){
      xhr.setRequestHeader("Accept","application/json");
       return xhr.setRequestHeader('X-CSRFToken', $("#csrf_token").val());
    },
    error: function(xhr) {
      console.log("response.statusMessage>>>"+JSON.parse(xhr.responseText).statusMessage);
    }
  }).then(function(finalResponse) {
      try{
        if(finalResponse != "" || finalResponse != "undefined" || finalResponse != undefined || finalResponse != null){
          if($.parseJSON(finalResponse).events.length == 0){
            showNoDataMessage(chartDetails);
          }else{
            if(chartDetails.chartType == "pie"){
              var obj = $.parseJSON(JSON.stringify($.parseJSON(finalResponse).events));
              var data = [];
              $.each(obj, function(key,value) {
                var item = {};
                item["label"] = value.label;
                item["value"] = parseInt(value.value);
                data.push(item);
              });
              generatePieChart(chartDetails, data);
            }else if(chartDetails.chartType == "bar"){
              var obj = $.parseJSON(JSON.stringify($.parseJSON(finalResponse).events));
              var labels = [];
              var values = [];
              $.each(obj, function(key,value) {
                if(parseInt(value.value) > 0){
                  if(value.label){
                    labels.push(value.label.match(/.{1,12}/g));
                  }else{
                    labels.push(value.label);
                  }
                  values.push(parseInt(value.value));
                }
              });
              generateBarChart(chartDetails, labels, values);
            }
          }
        }else{
          setTimeout(function(){ getResult(chartDetails, searchId);}, 3000);
        }
      }catch(ex){
        console.log("Exception while returing MDM offline response>>>"+ex.message);
      }
  });
}
function getsearchIdForGrid(chartDetails, label) {
  if(chartDetails.url === "mdm_offline_grid"){
    generateGridMDMOfflineGrid(chartDetails, label);
  }else{
    var beforeSevenDays = $('#IDOfDateRangePicker').data('daterangepicker').startDate.format('YYYY-MM-DD HH:mm');
    var currentDate = $('#IDOfDateRangePicker').data('daterangepicker').endDate.format('YYYY-MM-DD HH:mm');
    $.ajax({   
      url: "../../"+chartDetails.appName+"/api/v1/range_search/call_to_all",
      type:'POST', 
      data: JSON.stringify({"query_param" : {"start_date" : beforeSevenDays, "end_date" : currentDate, "query_name" : chartDetails.url, "filters" :{"filter_field": chartDetails.fieldName, "filter_value": label}}}),
      contentType: "application/json; charset=utf-8",
      cache: false,
      beforeSend: function (xhr){ 
        xhr.setRequestHeader("Accept","application/json");
         return xhr.setRequestHeader('X-CSRFToken', $("#csrf_token").val());
      },
      error: function(xhr) {
        console.log("response.statusMessage>>>"+JSON.parse(xhr.responseText).statusMessage);
      }
    }).then(function(searchIdResponse) {
      getstatus(chartDetails, searchIdResponse.search_id);
    });
  }
}
function getstatus(chartDetails, searchId) {
  if (searchId != undefined || searchId == 'nill') {
    $.ajax({   
      url: "../../"+chartDetails.appName+"/api/v1/"+searchId+"/status",
      type:'GET', 
      contentType: "application/json; charset=utf-8",
      cache: false,
      beforeSend: function (xhr){ 
        xhr.setRequestHeader("Accept","application/json");
         return xhr.setRequestHeader('X-CSRFToken', $("#csrf_token").val());
      },
      error: function(xhr) {
        console.log("response.statusMessage>>>"+JSON.parse(xhr.responseText).statusMessage);
      }
    }).then(function(statusResponse) {
      if(statusResponse.status == "COMPLETED" && statusResponse.search_id != undefined){
        if(chartDetails.chartType != "grid"){
          getResult(chartDetails, statusResponse.search_id);
        }else{
          generateGrid(chartDetails, statusResponse.search_id);
        }
      }else{
        setTimeout(function(){ getstatus(chartDetails, statusResponse.search_id);}, 5000);
      }
    });
  }
}
function getResult(chartDetails, searchId) {
  if(chartDetails.chartType != "grid" && searchId != undefined){
    $.ajax({ 
      url: "../../"+chartDetails.appName+"/api/v1/"+searchId+"/result",
      type:'POST', 
      data: JSON.stringify({"result_response_type" : "2d"}),
      contentType: "application/json; charset=utf-8",
      cache: false,
      beforeSend: function (xhr){ 
        xhr.setRequestHeader("Accept","application/json");
         return xhr.setRequestHeader('X-CSRFToken', $("#csrf_token").val());
      },
      error: function(xhr) {
        console.log("response.statusMessage>>>"+JSON.parse(xhr.responseText).statusMessage);
      }
    }).then(function(finalResponse) {
      try{
        if(finalResponse.events.length == 0){
          showNoDataMessage(chartDetails);
        }else{
          if(chartDetails.chartType == "pie"){
            var obj = $.parseJSON(JSON.stringify(finalResponse.events));
            var data = [];
            $.each(obj, function(key,value) {
              var item = {};
              item["label"] = value.label;
              item["value"] = parseInt(value.value);
              data.push(item);
            });
            generatePieChart(chartDetails, data);
          }else if(chartDetails.chartType == "bar"){
            var obj = $.parseJSON(JSON.stringify(finalResponse.events));
            var labels = [];
            var values = [];
            $.each(obj, function(key,value) {
              if(parseInt(value.value) > 0){
                if(value.label){
                  labels.push(value.label.match(/.{1,12}/g));
                }else{
                  labels.push(value.label);
                }
               /* if(value.label){
                  labels.push(value.label.match(/.{1,25}/g));
                }else{
                  labels.push(value.label);
                }*/
                values.push(parseInt(value.value));
              }
            });
            generateBarChart(chartDetails, labels, values);
          }
        }   
      }catch(ex){
        console.log("Exception while returing response>>>"+ex.message);
      }
    });
  }
}
function showNoDataMessage(chartDetails){
  if(chartDetails.chartType == "pie"){
    $("#"+chartDetails.title).html("");
    $("#"+chartDetails.title).html('<span style="font:18px verdana;margin-left:20px;">No data to display</span>');     
  }else if(chartDetails.chartType == "bar"){
    document.getElementById(chartDetails.title).getContext("2d").clearRect(0, 0, 100, 50);
    document.getElementById(chartDetails.title).getContext("2d").font = '18px verdana';
    document.getElementById(chartDetails.title).getContext("2d").fillText('No data to display',10,40);
  }  
}
function generateBarChart(chartDetails, labels, values){
  $("#"+chartDetails.title).empty();
  new Chart(document.getElementById(chartDetails.title), {
    type: 'bar',
    data: {
      labels: labels,
      datasets: [{
        backgroundColor: ["#3366CC","#DC3912","#FF9900","#109618","#990099","#3B3EAC","#0099C6","#DD4477","#66AA00","#B82E2E","#316395","#994499","#22AA99","#AAAA11","#6633CC","#E67300","#8B0707","#329262","#5574A6","#3B3EAC"],
        data: values,
      }],
      labelLength: 1,
    },
    options: {
      maintainAspectRatio: false,
      responsive: true,
      scales: {
        xAxes: [{
          time: {
            unit: 'count'
          },
          gridLines: {
            display: false
          },
          ticks: {
            autoSkip: false,
            callback: function(value, index, values) {
              if (value.toString().replace(/,/g, '').length > 26)
                return value.toString().replace(/,/g, '').substring(0, 26)+'...';
              else
                return value;
            }
          }
        }],
        yAxes: [{
          ticks: {
            min: 0,
            max: parseInt(Math.max.apply(Math,values)+50),
            maxTicksLimit: 5
          },
          gridLines: {
            display: false
          }
        }],
      },
      legend: {
        display: false,
        position: 'bottom',
        labels: {
          display: false,
          boxWidth:8
        }
      },
      animation: {
        duration: 0,
        onComplete: function () {
          var ctx = this.chart.ctx;
          ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, 'normal', Chart.defaults.global.defaultFontFamily);
          ctx.fillStyle = this.chart.config.options.defaultFontColor;
          ctx.textAlign = 'center';
          ctx.textBaseline = 'bottom';
          this.data.datasets.forEach(function (dataset) {
            for (var i = 0; i < dataset.data.length; i++) {
              var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
              ctx.fillText(dataset.data[i], model.x, model.y - 5);
            }
          });
        }
      },
      onClick:function(c,i){
        e = i[0];
        var x_value = this.data.labels[e._index];
        var y_value = this.data.datasets[0].data[e._index];
        loadDrillDown(x_value.toString().replace(/,/g, ''), y_value, chartDetails.title);
      }
    }
  });
}
function generatePieChart(chartDetails, data){
  $("#"+chartDetails.title).empty();
  new d3pie(chartDetails.title, {
    labels: {
      inner: {
        format: "value"
      }
    },
    size: {
      pieInnerRadius: "50%",
      canvasHeight: 330,
      canvasWidth: 590
    },
    data: {
      content: data
    },
    tooltips: {
      enabled: true,
      type: "placeholder",
      string: "{label}: {value}",
      styles: {
        fadeInSpeed: 100,
        backgroundColor: "#000000",
        backgroundOpacity: 0.9,
        color: "#ffffff",
        borderRadius: 2,
        font: "verdana",
        fontSize: 10,
        padding: 5
      }
    },
    callbacks: {
      onClickSegment: function(e) {
        var x_value = e.data.label
        var y_value = e.data.value;
        loadDrillDown(x_value.toString().replace(/,/g, ''), y_value, chartDetails.title);
        //console.log(a);
      }
    }
  });
}
function generateGrid(chartDetails, searchId){
  $("#tblGrid").empty();
  var table = $('#tblGrid').DataTable({
    "pageLength": 10, 
    "processing": true,
    "serverSide": true,
    "searching": false,
    "columns": chartDetails.fieldLabels,
    "ajax" : {
      'url': "../../"+chartDetails.appName+"/api/v1/"+searchId+"/result",
      'type' : "POST",
      'beforeSend': function (xhr){
        xhr.setRequestHeader("Accept","application/json");
         return xhr.setRequestHeader('X-CSRFToken', $("#csrf_token").val());
      },
      "data" : function(args) {
        var table = $('#tblGrid').DataTable();
        var tablePageInfo = table.page.info();
        var order = table.order();
        args.page_number = parseInt(parseInt(tablePageInfo.page) + 1);
        args.page_size = args.length;
        args.sort_field = $(table.column(order[0][0]).header()).html();
        args.sort_dir = order[0][1];
        args.result_response_type = "grid";
        return JSON.stringify(args);
      },
      "contentType": "application/json; charset=utf-8"
    },
    "orderCellsTop": true,
    "scrollX": true
  });
  $("#export").css("display", "block");
  $("#export").attr("href", "../../"+chartDetails.appName+"/api/v1/"+searchId+"/export");
  loadPolicies();
}
function generateGridMDMOfflineGrid(chartDetails, label){
  $("#tblGrid").empty();
  var table = $('#tblGrid').DataTable({
    "pageLength": 10, 
    "processing": true,
    "serverSide": true,
    "searching": false,
    "columns": chartDetails.fieldLabels,
    "ajax" : {
      'url': "../../"+chartDetails.appName+"/api/v1/getMDMEndpoints_drill",
      'type' : "POST",
      'beforeSend': function (xhr){
        xhr.setRequestHeader("Accept","application/json");
         return xhr.setRequestHeader('X-CSRFToken', $("#csrf_token").val());
      },
      "data" : function(args) {
        var table = $('#tblGrid').DataTable();
        var tablePageInfo = table.page.info();
        var order = table.order();
        args.page_number = parseInt(parseInt(tablePageInfo.page) + 1);
        args.page_size = args.length;
        args.sort_field = $(table.column(order[0][0]).header()).html();
        args.sort_dir = order[0][1];
        args.filter_field = chartDetails.fieldName;
        args.filter_value = label;
        args.sort_dir = order[0][1];
        return JSON.stringify({"query_param" : args});
      },
      "contentType": "application/json; charset=utf-8"
    },
    "orderCellsTop": true,
    "scrollX": true
  });
  $("#export").css("display", "none");
}
function loadPolicies() {
  var policyList = {};
  $.ajax({
    url: "../../cisco/api/v1/anc_getPolicies",
    type:'GET', 
    contentType: "application/json; charset=utf-8",
    cache: false,
   beforeSend: function (xhr){
        xhr.setRequestHeader("Accept","application/json");
         return xhr.setRequestHeader('X-CSRFToken', $("#csrf_token").val());
      },
    error: function(xhr) {
      console.log("response.statusMessage>>>"+JSON.parse(xhr.responseText).statusMessage);
    }
  }).then(function(policyLists) {
    if(policyList != "null"){
      $.each(JSON.parse(policyLists).policies, function( index, value ) {
        var policyName = {};
        policyName["name"] = value.name;
        policyList[value.name] = policyName;
      });
      $.contextMenu({
        selector: '.dataTable td.mac, td.ip',

        build: function($trigger, e) {
          var options = {
            className: 'data-title',
            callback: function(key, options) {
              var data = {};
              data["context"] = options.$trigger.text();
              data["type"] = options.$trigger.attr('class').substr(0, 3).trim();
              data["policyName"] = key;
              performAction(data);
            },
            items: policyList
          };
          /*if($trigger.hasClass('mac')) {
            options.items.pxGridQRadarClear = {name: "pxGridQRadarClear"};
          }else{
            delete options.items.pxGridQRadarClear;
          }*/
          return options;
        }
      });
    }
  });
}
function performAction(data) {
  $.ajax({   
    url  : "../../cisco/api/v1/anc_action",
    type :'POST', 
    data : JSON.stringify(data), 
    contentType: "application/json; charset=utf-8",
    cache: false,
   beforeSend: function (xhr){
        xhr.setRequestHeader("Accept","application/json");
         return xhr.setRequestHeader('X-CSRFToken', $("#csrf_token").val());
      },
    error: function(xhr) {
      console.log("response.statusMessage>>>"+JSON.parse(xhr.responseText).statusMessage);
    }
  }).then(function(statusResponse) {
    statusResponse = JSON.parse(statusResponse);
    if(statusResponse.failureReason){
      alert("Status : "+statusResponse.status+"\nOperation Id : "+statusResponse.operationId+"\nFailure Reason : "+statusResponse.failureReason);
    }else{
      alert("Status : "+statusResponse.status+"\nOperation Id : "+statusResponse.operationId);
    }
  });
}
function generateStackedBarChart(chartDetails, searchId){
  // Return with commas in between
  var numberWithCommas = function(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  };
  $("#"+chartDetails.title).empty();
  new Chart(document.getElementById(chartDetails.title), {
    type: 'bar',
    data: {
      labels: ["A", "B", "C"],
      datasets: [{
        label: 'Apple',
        data: [5,0,0],
        backgroundColor: "#109618"
      },
      {
        label: 'Ball',
        data: [2,3,1],
        backgroundColor: "#3B3EAC"
      },
      {
        label: 'Cat',
        data: [0,0,3],
        backgroundColor: "#DD4477"
      }]
    },
    options: {
      animation: {
        duration: 10,
      },
      tooltips: {
        mode: 'label',
        callbacks: {
          label: function(tooltipItem, data) { 
            return data.datasets[tooltipItem.datasetIndex].label+ ": "+numberWithCommas(tooltipItem.yLabel);
          }
        }
      },
      scales: {
        xAxes: [{ 
          stacked: true, 
          gridLines: { display: false },
        }],
        yAxes: [{ 
          stacked: true, 
          ticks: {
            callback: function(value) { return numberWithCommas(value); },
          }, 
        }],
      },
      legend: {
        display: true,
        position: 'bottom',
        labels: {
          boxWidth : 12
        }
      }
    }
  });
}