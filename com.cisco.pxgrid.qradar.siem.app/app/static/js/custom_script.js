
$( document ).ready( function() {
    /* Subscribe for selection changes on the offenses table */
    domapi.addEvent( defaultTable, "selectionchanged", dt_app_toolbar_enable);
} );

$( window ).bind( "toolbarLoaded", dt_app_toolbar_enable );
function dt_app_toolbar_enable() {
    /* Enable the escalation button if a single offense is selected */
    var isOK = ( pageId == "OffenseList" ) || ( selectedRows.length == 1 );
    setActionEnabled( "offense_toolbar_button_action", isOK );
}

function offense_toolbar_button_action(result) {
	  //var appId = context.appId;
      var rowid;
	  if( pageId === "OffenseList" ) {
        // Source is the offense list
        if( selectedRows.length === 0) {
            alert("Please select an offense.");
            return;
        }
        else if ( selectedRows.length > 1) {
            alert("Please select a single offense.");
            return;
        }
        rowid = selectedRows[0].id;

    } else {
        // Source is the offense details page
       // offenseId = summaryId;
		  alert("Nothing to do here");
    }

	alert(JSON.stringify(result));
}