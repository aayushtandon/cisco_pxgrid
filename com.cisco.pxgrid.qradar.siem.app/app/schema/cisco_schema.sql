
create table active_server (
  id integer primary key autoincrement,
  key text not null,
  switch_status text,
  updated_date TIMESTAMP not null
);

create table job_status_table (
  id integer primary key autoincrement,
  current_job_status text not null,
  updated_date TIMESTAMP not null
);

create table system_setting (
  id integer primary key autoincrement,
  key text not null,
  key_label text not null,
  value text,
  section text NOT NULL,
  object_type text NOT NULL,
  display_order INTEGER NOT NULL ,
  data text,
  updated_date TIMESTAMP not null
);

create table encryption(
  id integer primary key,
  enc_key text NOT NULL
);

INSERT INTO active_server (key,switch_status,updated_date) VALUES ('active_server','primary', DateTime('now'));
INSERT INTO job_status_table (current_job_status,updated_date) VALUES ('stopped', DateTime('now'));
INSERT INTO system_setting (key,key_label,value,section,object_type,display_order,data,updated_date) VALUES ('qradar_api_user_token', 'QRadar service token','','ApplicationSettings','text', 1, 'null', DateTime('now'));
INSERT INTO system_setting (key,key_label,value,section,object_type,display_order,data,updated_date) VALUES ('job_execution_interval_in_minutes','Time interval to invoke the scheduler in minutes','5','ApplicationSettings','integer',2,'null', DateTime('now'));
INSERT INTO system_setting (key,key_label,value,section,object_type,display_order,data,updated_date) VALUES ('cisco_app_version','Cisco application version','v1','ApplicationSettings','text',3,'null',DateTime('now'));

INSERT INTO system_setting (key,key_label,value,section,object_type,display_order,data,updated_date) VALUES ('primary_pxgrid_server','Primary pxGrid Server IP Address','','primary','text',4,'null', DateTime('now'));
INSERT INTO system_setting (key,key_label,value,section,object_type,display_order,data,updated_date) VALUES ('primary_port','port','8910','primary','integer',5,'null', DateTime('now'));
INSERT INTO system_setting (key,key_label,value,section,object_type,display_order,data,updated_date) VALUES ('primary_client_username','Client user name','','primary','text',6,'null', DateTime('now'));
INSERT INTO system_setting (key,key_label,value,section,object_type,display_order,data,updated_date) VALUES ('primary_file_upload','Select & Upload certificates(only PEM is supported)','','primary','file',7,'null', DateTime('now'));
INSERT INTO system_setting (key,key_label,value,section,object_type,display_order,data,updated_date) VALUES ('primary_client_cert_pem_filepath','Certificate file name','','primary','text',8,'null', DateTime('now'));
INSERT INTO system_setting (key,key_label,value,section,object_type,display_order,data,updated_date) VALUES ('primary_client_cert_key_filepath','Certificate key file name','','primary','text',9,'null', DateTime('now'));
INSERT INTO system_setting (key,key_label,value,section,object_type,display_order,data,updated_date) VALUES ('primary_ca_cert_filepath','Root CA certificate file name','','primary','text',10,'null', DateTime('now'));

INSERT INTO system_setting (key,key_label,value,section,object_type,display_order,data,updated_date) VALUES ('secondary_pxgrid_server','Secondary pxGrid Server IP Address','','secondary','text',1,'null', DateTime('now'));
INSERT INTO system_setting (key,key_label,value,section,object_type,display_order,data,updated_date) VALUES ('secondary_port','port','8910','secondary','integer',2,'null', DateTime('now'));
INSERT INTO system_setting (key,key_label,value,section,object_type,display_order,data,updated_date) VALUES ('secondary_client_username','Client user name','','secondary','text',3,'null', DateTime('now'));
INSERT INTO system_setting (key,key_label,value,section,object_type,display_order,data,updated_date) VALUES ('secondary_file_upload','Select & Upload certificates(only PEM is supported)','','secondary','file',4,'null', DateTime('now'));
INSERT INTO system_setting (key,key_label,value,section,object_type,display_order,data,updated_date) VALUES ('secondary_client_cert_pem_filepath','Certificate file name','','secondary','text',5,'null', DateTime('now'));
INSERT INTO system_setting (key,key_label,value,section,object_type,display_order,data,updated_date) VALUES ('secondary_client_cert_key_filepath','Certificate key file name','','secondary','text',6,'null', DateTime('now'));
INSERT INTO system_setting (key,key_label,value,section,object_type,display_order,data,updated_date) VALUES ('secondary_ca_cert_filepath','Root CA certificate file name','','secondary','text',7,'null', DateTime('now'));
