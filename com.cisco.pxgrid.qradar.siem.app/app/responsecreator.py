__author__ = 'Aujas'


import json
from flask import Flask
from flask_wtf.csrf import CSRFProtect
from qpylib import qpylib

from flask import Response


class ResponseHandler:
    def __init__(self):

        self.app = Flask(__name__)
        csrf = CSRFProtect(self.app)
        csrf.init_app(self.app)

    #Yes i know we could have used the flask-json module. But i just didnt like to add more modules and make this heavier.
    #our need is very simple so let us keep it small and simple
    def get_success_response(self,data,total_records=None,cache=None):
        if data == None:
            data={'cors':"Allowed"}
        resp = Response(response=json.dumps(data, default=self.obj_dict),status=200, mimetype="application/json")
        h = resp.headers
        # prepare headers for CORS in dev mode so we can run the JS from node server
        if qpylib.is_sdk() == True:
            h['Access-Control-Allow-Origin'] = '*'
            h['Access-Control-Allow-Methods'] = 'GET,POST,PUT,OPTIONS'
            h['Access-Control-Allow-Headers'] = 'Origin, X-Requested-With, Content-Type, Accept'
            h['Access-Control-Expose-Headers'] = 'total_records'
        if (cache == None or cache == False):
            h['Cache-Control'] = 'must-revalidate, no-cache, no-store'
        if not total_records == None:
            h['total_records'] = total_records
        h['Connection'] = 'Keep-Alive'
        h['Keep-Alive'] = 'timeout=300, max=99'
        resp.headers = h
        return resp

    def get_forbidden_response(self,data):
        return Response(status=401, mimetype="application/json")

    #let us create a proper response later
    #TODO: Ensure proper response message is sent
    def get_server_internal_error_response(self,data):
        return Response(response=json.dumps(data),status=500, mimetype="application/json")

    #let us create a proper response later
    #TODO: Ensure proper response message is sent
    def get_data_failed_response(self,data):
        return Response(response=json.dumps(data),status=412, mimetype="application/json")


    def obj_dict(self,obj):
        return obj.__dict__
