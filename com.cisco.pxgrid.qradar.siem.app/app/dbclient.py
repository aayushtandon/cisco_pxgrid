__author__ = 'Aujas'

from qpylib import qpylib
from flask import Flask
import os
import os.path
import logging
from datetime import datetime, timedelta
from contextlib import closing
import sqlite3

logging.getLogger().setLevel(logging.INFO)

from flask_wtf.csrf import CSRFProtect


class DBClient:

    def __init__(self):
        if qpylib.is_sdk():
            DEBUG = True

        self.DATABASE = qpylib.get_store_path("pxGridstore.db")
        self.app = Flask(__name__)

        csrf = CSRFProtect(self.app)
        csrf.init_app(self.app)

        # Call upon load
        self.start_db()


    # Connect to the DB
    def connect_db(self):
        return sqlite3.connect(self.DATABASE,timeout=2)


    # initialize or setup DB
    def init_db(self):
        with closing(self.connect_db()) as db:
            with self.app.open_resource('schema/cisco_schema.sql', mode='r') as f:
                db.cursor().executescript(f.read())
                # app.logger("executing the file")
            db.commit()


    # Start the Database
    def start_db(self):
        if not os.path.isfile(self.DATABASE):
            self.init_db()


    # The following method is to update  the system settings value
    def update_system_setting(self,params):
        idval = int(params['id'])
        updatedval = params['value']
        db = self.connect_db()
        query_string = 'update system_setting set value=? , updated_date=DateTime(\'now\') where id=?'
        cur = db.execute(query_string, [updatedval,idval])
        db.commit()
        if cur is not None:
            cur.close()
        query_string = 'select id, key , key_label, value, updated_date from system_setting where id =?'
        data = db.execute(query_string,[idval])
        setting = [dict(id=row[0], key=row[1], key_label=row[2], value=row[3], updated_date=row[4]) for row in data.fetchall()]
        if data is not None:
            data.close()
        if db is not None:
            db.close()
        return setting


# The following method is to get all the system settings
    def list_system_settings(self):
        db = self.connect_db()

        query_string = 'select id, key, key_label, value, section, object_type, display_order, data, updated_date from system_setting order by id'
        cur = db.execute(query_string)

        setting = [dict(id=row[0], key=row[1], key_label=row[2], value=row[3], section=row[4], object_type=row[5], display_order=row[6], data=row[7], updated_date=row[8]) for row in cur.fetchall()]
        
        if cur is not None:
            cur.close()

        if db is not None:
            db.close()

        for i in setting:
            if i[u'key_label'] in [u'QRadar service token'] and i[u'value']!='':
                i[u'value'] = '**************'

        return setting


    def get_system_setting(self,key):
        db = self.connect_db()

        query_string = 'select key, value from system_setting where key=?'
        cur = db.execute(query_string,[key])
        row = cur.fetchone()
        setting = dict(key=row[0], value=row[1])
        if cur is not None:
            cur.close()
        if db is not None:
            db.close()
        return setting


    def get_system_setting_id(self,key):
        db = self.connect_db()
        query_string = 'select id, value from system_setting where key=?'
        cur = db.execute(query_string,[key])
        row = cur.fetchone()
        setting = dict(key=row[0], value=row[1])
        if cur is not None:
            cur.close()
        if db is not None:
            db.close()
        return setting


    def get_system_settings(self):
        db = self.connect_db()
        query_string = 'select key , value from system_setting'
        cur = db.execute(query_string)
        setting = [dict(key=row[0], value=row[1]) for row in cur.fetchall()]
        if cur is not None:
            cur.close()
        if db is not None:
            db.close()
        return setting


    # The following method is to update and presist the active server  details
    def upsert_active_server_details(self, params):
        if params is not None and len(params) > 0:
            db = self.connect_db()
            _current_datetime = datetime.utcnow()
            _id = 1
            _key = 'active_server'
            _switch_status = params['switch_status']
            cur = None
            try:
                cur = db.execute('INSERT OR REPLACE INTO active_server (id,key,switch_status,updated_date) VALUES (?, ?, ?, ?)',[_id, _key, _switch_status, _current_datetime])
            except Exception as exp:
                qpylib.log('Error: Unable to update the servers status')
                qpylib.log(str(exp))
            lid = cur.lastrowid
            db.commit()
            if db is not None:
                db.close()
        query_string = 'select switch_status from active_server order by updated_date desc'
        db = self.connect_db()
        data = db.execute(query_string)
        active_server = [dict(switch_status=row[0]) for row in data.fetchall()]
        if data is not None:
            data.close()
        if db is not None:
            db.close()
        qpylib.log('Swapped server details ..............'+str(active_server))
        return active_server[0]

    # The following method is to update and presist the active server  details
    def get_active_server_details(self):
        db = self.connect_db()
        query_string = 'select switch_status from active_server order by updated_date desc'
        data = db.execute(query_string)
        active_server = [dict(switch_status=row[0]) for row in data.fetchall()]
        if data is not None:
            data.close()
        if db is not None:
            db.close()
        return active_server[0]


    # The following method is to update and presist the job status
    def updatejob_status(self, params):
        if params is not None and len(params) > 0:
            db = self.connect_db()
            _current_datetime = datetime.utcnow()
            _id = 1
            qpylib.log('Current job status: {0}'.format(str(params)))
            _current_job_status = params['current_job_status']
            cur = None
            try:
                cur = db.execute('INSERT OR REPLACE INTO job_status_table (id, current_job_status, updated_date) VALUES (?, ?, ?)', [_id, _current_job_status, _current_datetime])
            except Exception as exp:
                qpylib.log('Error: Unable to update the job status')
                qpylib.log(str(exp))
            lid = cur.lastrowid
            db.commit()
            if db is not None:
                db.close()
        query_string = 'select current_job_status from job_status_table order by updated_date desc'
        db = self.connect_db()
        data = db.execute(query_string)
        current_job_status = [dict(current_job_status=row[0]) for row in data.fetchall()]
        if data is not None:
            data.close()
        if db is not None:
            db.close()
        qpylib.log('Job status updated..............{0}'.format(str(current_job_status[0])))
        return current_job_status[0]

    # The following method is to get the current job status
    def getjobststus(self):
        db = self.connect_db()
        query_string = 'select current_job_status from job_status_table order by updated_date desc'
        data = db.execute(query_string)
        current_job_status = [dict(current_job_status=row[0]) for row in data.fetchall()]
        if data is not None:
            data.close()
        if db is not None:
            db.close()
        return current_job_status[0]

    def timedelta_total_seconds(self, timedelta):
        return (timedelta.microseconds + 0.0 + (timedelta.seconds + timedelta.days * 24 * 3600) * 10 ** 6) / 10 ** 6

    def inserting_encryption_database(self,enc_key):
        try:
            db = self.connect_db()
            sql = 'INSERT OR REPLACE INTO encryption (id,enc_key) VALUES (?,?)'
            cur = db.execute(sql, [1,enc_key])
            if cur is not None:
                db.commit()
                cur.close()
            if db is not None:
                db.close()
        except Exception as db_insert_error:
            qpylib.log('db_insert_error:- '+str(db_insert_error), level='ERROR')
        return

    def retrieving_encryption_database(self):
        result_set = []
        try:
            db = self.connect_db()
            query_string = 'select * from encryption'
            cur1 = db.execute(query_string)
            row = cur1.fetchall()
            result_set = row
            if cur1 is not None:
                db.commit()
                cur1.close()
            if db is not None:
                db.close()
        except Exception as db_retrieve_error:
            qpylib.log('db_retrieve_error:- ' + str(db_retrieve_error), level='ERROR')
        return result_set
