__author__ = 'Aujas'

from flask_wtf.csrf import CSRFProtect
import configparser
import os
from app import app
from magic import Magic
from app_proxyservice import Service
import json
from flask import request, render_template, abort, stream_with_context, Response
from qpylib import qpylib
from werkzeug.utils import secure_filename
from arielsearchclient import ArielSearchAPIClient
from query_helper import QueryHelper
import operator
import itertools
import logging
import zipfile
import time
import datetime
import shutil
import validator
from werkzeug.wrappers import BaseResponse
import re

logging.getLogger().setLevel(logging.INFO)


csrf = CSRFProtect(app)
csrf.init_app(app)

ALLOWED_EXTENSIONS = set(['cer', 'key'])
PRIMARY_UPLOAD_FOLDER = 'store/primary'
app.config['PRIMARY_UPLOAD_FOLDER'] = PRIMARY_UPLOAD_FOLDER
SECONDARY_UPLOAD_FOLDER = 'store/secondary'
app.config['SECONDARY_UPLOAD_FOLDER'] = SECONDARY_UPLOAD_FOLDER
file_path = os.getcwd()
queryfile = str(file_path) + "/app/qradar_queries.ini"
messge_ojb = {"Message": "Invalid input"}


# Every relevant request will be checked for the CSRF header
@app.before_request
def check_csrf():
    csrf.protect()


def aql_dictfunc():
    config = configparser.RawConfigParser()
    config.read(queryfile)
    return dict(config.items("queries"))


aql_dict = aql_dictfunc()


def validate_inputs(param1, param2):
    isValid = True
    if param1 == None or param1 == 'undefined' or param1.strip() == '':
        isValid = False
    if param2 == None or param2 == 'undefined' or param2.strip() == '':
        isValid = False
    return isValid


def manageQRadarLatency(_time, timeType):
    '''
    pattern = '%Y-%m-%d %H:%M'
    epoch = int(time.mktime(time.strptime(_time, pattern)))
    if timeType == 'start':
        #logging.info("Old start time is:"+ _time)
        #Substract 1/2 day
        epoch = epoch - 43200
    else:
        #logging.info("Old end is:"+ _time)
        #Add 1/2 day
        epoch = epoch + 43200
    #buffered_date = datetime.datetime.strptime(str(epoch), pattern)
    buffered_date = time.strftime(pattern, time.localtime(epoch))
    #logging.info("New time is:"+ buffered_date)
    '''
    return _time


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@app.route('/')
@app.route('/index')
def index():
    projectservice = Service()
    qradar_api_instance = qpylib.strategy()
    return render_template("index.html", base_url=qradar_api_instance.get_app_base_url())


@app.route('/app_settings')
@app.route('/appsettings')
@app.route('/application_settings')
@app.route('/applicationsettings')
def app_settings():
    qradar_api_instance = qpylib.strategy()
    return render_template("application_settings.html", base_url=qradar_api_instance.get_app_base_url())


@app.route('/cisco/api/v1/system/settings/list', methods=['GET'])
def application_settings_list():
    qpylib.log("Received request to list settings page")
    projectservice = Service()
    obj = projectservice.application_settings_list()
    return json.dumps(obj)


@app.route('/cisco/api/v1/system/settings/update', methods=['POST'])
def application_settings_update():
    qpylib.log("Received request to update system settings")
    projectservice = Service()

    all_args = request.get_json(silent=True)
    qpylib.log('all_args>>>>>>>'+str(all_args))
    for i in all_args:
        if i[u'key_label'] == u'Primary pxGrid Server IP Address' or i[u'key_label'] == u'Secondary pxGrid Server IP Address':
            #qpylib.log('the returned value>>>>>>'+str(validator.ip_validator(i[u'value'])))
            if i[u'value']!= '':
                if not validator.ip_validator(i[u'value']):
                    raise Exception('Enter Valid IP Address')
        else:
            if i[u'key_label'] != 'Select & Upload certificates(only PEM is supported)':
                #qpylib.log('the label>>>>>>>>>>>>>>.'+str(i[u'key_label']))
                if i[u'value'] != '' and i[u'value'] != '**************':
                    if not validator.cummulative_validator(i[u'value']):
                        raise Exception('Enter Valid Credentials')
    if not all_args:
        abort(400)
    obj = projectservice.application_settings_update(all_args)
    return json.dumps(obj)


@app.route('/cisco/api/v1/anc_quarantine', methods=['GET', 'POST'])
def anc_quarantine():
    qpylib.log("Calling anc_quarantine............")
    all_args = request.get_json(silent=True)
    response = perform_anc_action(all_args, 'pxGridQRadarQuarantine')
    return json.dumps(response)


@app.route('/cisco/api/v1/anc_shutdown', methods=['GET', 'POST'])
def anc_shutdown():
    qpylib.log("Calling anc_shutdown............")
    all_args = request.get_json(silent=True)
    response = perform_anc_action(all_args, 'pxGridQRadarShutDown')
    return json.dumps(response)


@app.route('/cisco/api/v1/anc_portbounce', methods=['GET', 'POST'])
def anc_portbounce():
    qpylib.log("Calling anc_portbounce............")
    all_args = request.get_json(silent=True)
    response = perform_anc_action(all_args, 'pxGridQRadarPortBounce')
    return json.dumps(response)


@app.route('/cisco/api/v1/anc_clear', methods=['GET', 'POST'])
def anc_clear():
    qpylib.log("Calling anc_clear............")
    all_args = request.get_json(silent=True)
    projectservice = Service()
    projectservice.initializeService()

    context = str(all_args['context'])
    if not validator.cummulative_validator(context):
        raise Exception('validation error !!!')

    cxt_type = str(all_args['type'])
    if not validator.cummulative_validator(cxt_type):
        raise Exception('validation error !!!')

    if cxt_type == 'mac':
        payload = json.dumps({'policyName': 'pxGridQRadarClear', 'macAddress': context})
        response = projectservice.invoke_cisco_api('com.cisco.ise.config.anc', 'clearEndpointByMacAddress', payload)
    else:
        response = json.dumps({'message': "Invalid column to perform ANC action"})

    return json.dumps(response)


@app.route('/cisco/api/v1/anc_action', methods=['GET', 'POST'])
def anc_action():
    qpylib.log("Calling anc_action............")
    all_args = request.get_json(silent=True)
    response = perform_anc_action_dynamic(all_args)
    return json.dumps(response)


@app.route('/cisco/api/v1/radius_failure', methods=['GET'])
def radius_failure():
    projectservice = Service()
    service_name = 'com.cisco.ise.radius'
    payload = json.dumps({})

    startTimestamp = request.args.get('starttime')
    qpylib.log("startTimestamp---------->>>>"+str(startTimestamp))
    if not validator.date_validator(startTimestamp):
       raise Exception('Enter Valid Date')

    if startTimestamp != None and startTimestamp not in 'null':
        payload = json.dumps({'startTimestamp': startTimestamp})
    response = projectservice.invoke_cisco_api(service_name, 'getFailures', payload)
    return json.dumps(response)


@app.route('/cisco/api/v1/getFailureById', methods=['GET'])
def getFailureById():
    projectservice = Service()
    service_name = 'com.cisco.ise.radius'

    entity_id = request.args.get('entity_id')
    if not validator.cummulative_validator(entity_id):
        raise Exception('validation error !!!')

    payload = json.dumps({'id': entity_id})
    response = projectservice.invoke_cisco_api(service_name, 'getFailureById', payload)
    return json.dumps(response)


@app.route('/cisco/api/v1/getProfiles', methods=['GET'])
def getProfiles():
    projectservice = Service()
    service_name = 'com.cisco.ise.config.profiler'
    payload = json.dumps({})
    response = projectservice.invoke_cisco_api(service_name, 'getProfiles', payload)
    return json.dumps(response)


@app.route('/cisco/api/v1/anc_getPolicies', methods=['GET'])
def anc_getPolicies():
    projectservice = Service()
    service_name = 'com.cisco.ise.config.anc'
    payload = json.dumps({})
    response = projectservice.invoke_cisco_api(service_name, 'getPolicies', payload)
    return json.dumps(response)


@app.route('/cisco/api/v1/getMDMEndpoints', methods=['GET'])
def getMDMEndpoints():
    projectservice = Service()
    service_name = 'com.cisco.ise.mdm'
    payload = json.dumps({})
    response = projectservice.invoke_cisco_api(service_name, 'getEndpoints', payload)
    return json.dumps(response)


@app.route('/cisco/api/v1/getMDMEndpoints_offline/<name>', methods=['GET'])
def getMDMEndpoints_offline(name):
    projectservice = Service()
    service_name = 'com.cisco.ise.mdm'
    payload = json.dumps({})
    response1 = {}
    response = projectservice.invoke_cisco_api(service_name, 'getEndpoints', payload)
    qpylib.log(str(response))
    if response is None:
        return json.dumps({"events": []})
    # response = data_req
    list1 = []
    list_req = []
    label = name
    list2 = response["endpoints"]
    list2.sort(key=operator.itemgetter(label))
    for key, items in itertools.groupby(list2, operator.itemgetter(label)):
        list1.append(list(items))

    for i in list1:
        graph_object = {}
        count = len(i)
        str1 = i[0][label]
        graph_object.update({"label": str1})
        graph_object.update({"value": count})
        list_req.append(graph_object)

    response1.update({'events': list_req})
    return json.dumps(response1)


@app.route('/cisco/api/v1/getMDMEndpoints_drill', methods=['POST'])
def getMDMEndpoints_drill():
    projectservice = Service()
    service_name = 'com.cisco.ise.mdm'
    payload = json.dumps({})
    response1 = {}
    response = projectservice.invoke_cisco_api(service_name, 'getEndpoints', payload)
    if response is None:
        return json.dumps({"events": []})
    # response = data_req
    all_args = request.get_json(silent=True)
    qpylib.log(str(all_args))
    query_param = all_args['query_param']

    filter_field = query_param["filter_field"]
    if not validator.cummulative_validator(filter_field):
        raise Exception('validation error !!!')

    filter_value = query_param["filter_value"]
    if not validator.cummulative_validator(filter_value):
        raise Exception('validation error !!!')

    page_number = query_param["page_number"]
    if not validator.cummulative_validator(page_number):
        raise Exception('validation error !!!')

    page_size = query_param["page_size"]
    if not validator.cummulative_validator(page_size):
        raise Exception('validation error !!!')

    sort_dir = query_param["sort_dir"]
    if not validator.cummulative_validator(sort_dir):
        raise Exception('validation error !!!')

    sort_field = query_param["sort_field"]
    if not validator.cummulative_validator(sort_field):
        raise Exception('validation error !!!')

    draw = query_param["draw"]
    if not validator.cummulative_validator(draw):
        raise Exception('validation error !!!')

    list1 = []
    list_req = []

    list2 = response["endpoints"]
    list2.sort(key=operator.itemgetter(filter_field))

    for key, items in itertools.groupby(list2, operator.itemgetter(filter_field)):
        list1.append(list(items))

    for i in list1:
        for j in i:
            if filter_field in j and j[filter_field] == filter_value:
                list_req.append(j)

    lenght_list = len(list_req)
    if page_number == None or page_number == 1:
        start_range = 0
        end_range = page_size
        qpylib.log("Page number is: " + str(page_number))
    else:
        qpylib.log("Page number is: " + str(page_number))
        end_range = (page_number * page_size)
        start_range = (end_range - page_size)
        if end_range > lenght_list:
            end_range = lenght_list

    if sort_dir == 'asc':
        newlist = sorted(list_req, key=operator.itemgetter(sort_field))
    else:
        newlist = sorted(list_req, key=operator.itemgetter(sort_field), reverse=True)

    newlist_grid = newlist[start_range:end_range]
    response1.update({'recordsTotal': lenght_list})
    response1.update({'recordsFiltered': lenght_list})
    response1.update({'data': newlist_grid})
    response1.update({'draw': draw})
    return json.dumps(response1)


@app.route('/cisco/api/v1/getEndpoints', methods=['GET'])
def getEndpoints():
    projectservice = Service()
    service_name = 'com.cisco.ise.config.anc'
    payload = json.dumps({})
    response = projectservice.invoke_cisco_api(service_name, 'getEndpoints', payload)
    return json.dumps(response)


@app.route('/cisco/api/v1/getMDMEndpointByMacAddresss', methods=['GET'])
def getMDMEndpointByMacAddresss():
    projectservice = Service()
    service_name = 'com.cisco.ise.mdm'

    mac_address = request.args.get('mac_address')
    # qpylib.log("mac_address--------->>>>>"+str(mac_address))
    if not validator.mac_validator(mac_address):
        raise Exception('Invalid MAC address')
    payload = json.dumps({'macAddress': mac_address})
    response = projectservice.invoke_cisco_api(service_name, 'getEndpointByMacAddress', payload)
    return json.dumps(response)


@app.route('/cisco/api/v1/getMDMEndpointsByType', methods=['GET'])
def getMDMEndpointsByType():
    projectservice = Service()
    service_name = 'com.cisco.ise.mdm'

    mdm_type = request.args.get('mdm_type')
    if not validator.cummulative_validator(mdm_type):
        raise Exception('validation error !!!')

    payload = json.dumps({'type': mdm_type})
    response = projectservice.invoke_cisco_api(service_name, 'getEndpointsByType', payload)
    return json.dumps(response)


@app.route('/cisco/api/v1/getMDMgetEndpointsByOsType', methods=['GET'])
def getMDMgetEndpointsByOsType():
    projectservice = Service()
    service_name = 'com.cisco.ise.mdm'

    osType = request.args.get('osType')
    if not validator.cummulative_validator(osType):
        raise Exception('validation error !!!')

    payload = json.dumps({'osType': osType})
    response = projectservice.invoke_cisco_api(service_name, 'getEndpointsByOsType', payload)
    return json.dumps(response)


@app.route('/cisco/api/v1/testssl', methods=['GET'])
def testssl():
    response = None
    qpylib.log("Testing pxGrid rest service using SSL certificate......")
    projectservice = Service()
    response = projectservice.testsscertificate('primary')
    return json.dumps(response)


@app.route('/cisco/api/v1/testws', methods=['GET'])
def testws():
    projectservice = Service()
    service_name = 'com.cisco.ise.pubsub'
    payload = json.dumps({})
    projectservice.invoke_cisco_ws_api(service_name, '/topic/com.cisco.ise.session', payload)

    return json.dumps({})


def perform_anc_action(all_args, _action):
    projectservice = Service()
    projectservice.initializeService()

    context = str(all_args['context'])
    if not validator.cummulative_validator(context):
        raise Exception('validation error !!!')

    cxt_type = str(all_args['type'])
    if not validator.cummulative_validator(cxt_type):
        raise Exception('validation error !!!')

    service_name = 'com.cisco.ise.config.anc'
    if cxt_type == 'ip':
        _endpoint = 'applyEndpointByIpAddress'
        payload = json.dumps({'policyName': _action, 'ipAddress': context})
        response = projectservice.invoke_cisco_api(service_name, _endpoint, payload)
    elif cxt_type == 'mac':
        _endpoint = 'applyEndpointByMacAddress'
        payload = json.dumps({'policyName': _action, 'macAddress': context})
        response = projectservice.invoke_cisco_api(service_name, _endpoint, payload)
    else:
        response = json.dumps({'message': "Invalid column to perform ANC action"})

    return response


def perform_anc_action_dynamic(all_args):
    projectservice = Service()
    projectservice.initializeService()

    context = str(all_args['context'])
    if not validator.cummulative_validator(context):
        raise Exception('validation error !!!')

    cxt_type = str(all_args['type'])
    if not validator.cummulative_validator(cxt_type):
        raise Exception('validation error !!!')

    policyName = str(all_args['policyName'])
    if not validator.cummulative_validator(policyName):
        raise Exception('validation error !!!')

    service_name = 'com.cisco.ise.config.anc'
    if cxt_type == 'ip':
        _endpoint = 'applyEndpointByIpAddress'
        payload = json.dumps({'policyName': policyName, 'ipAddress': context})
        response = projectservice.invoke_cisco_api(service_name, _endpoint, payload)
    elif cxt_type == 'mac':
        _endpoint = 'applyEndpointByMacAddress'
        payload = json.dumps({'policyName': policyName, 'macAddress': context})
        if policyName == 'pxGridQRadarClear':
            response = projectservice.invoke_cisco_api('com.cisco.ise.config.anc', 'clearEndpointByMacAddress', payload)
        else:
            response = projectservice.invoke_cisco_api(service_name, _endpoint, payload)
    else:
        response = json.dumps({'message': "Invalid column to perform ANC action"})

    return response

def func_re(list_main):
    str_main = ''
    for i in list_main:
        str_main = str_main+i
    allowed = re.compile("[+=/A-Z\s\d]", re.IGNORECASE)
    return all(allowed.match(x) for x in str_main)

@app.route('/cisco/api/v1/system/settings/update/primary/files', methods=['POST'])
def application_settings_update_primary_files():
    #mgic = Magic(magic_file = 'C:\Windows\System32\magic.mgc', mime=True)
    mgic = Magic()
    projectservice = Service()
    response_body = ""
    qpylib.log("Received request to update system settings files")
    # import _io
    # _io.FileIO
    # _io.BytesIO
    try:
        _desdir = os.path.join(app.config['PRIMARY_UPLOAD_FOLDER'],
                               'PrimaryCertificates%s' % datetime.datetime.now().strftime("%Y%m%d"))
        if not os.path.exists(_desdir):
            os.makedirs(_desdir)

        for prim_file in os.listdir(app.config['PRIMARY_UPLOAD_FOLDER']):
            if prim_file.endswith(".cer"):
                with open(os.path.join(app.config['PRIMARY_UPLOAD_FOLDER'], prim_file), 'r') as infile:
                    shutil.copy2(os.path.join(app.config['PRIMARY_UPLOAD_FOLDER'], prim_file), _desdir)
        prim_file = request.files.get('file')
        iobytesiovalue = prim_file.getvalue()
        # qpylib.log('iobytesiovalue>>>>>>>>>>>>>' + str(iobytesiovalue))
        iobytesiovalue_list = iobytesiovalue.split('\n')
        # qpylib.log('iobytesiovalue_list>>>>>>>>>>>>>'+str(iobytesiovalue_list))
        # fileiovalue = _io.FileIO.readinto(prim_file,1024)
        # qpylib.log('fileiovalue>>>>>>>>>>>>>' + str(fileiovalue))
        # prim_file_text = prim_file.read()
        # qpylib.log('prim_file_text>>>>>>>>>>>>>'+str(prim_file_text))
        # prim_file2 = BaseRequest._get_file_stream(prim_file,1024,mgic.from_buffer(prim_file))
        # qpylib.log('the stream>>>>>>'+str(prim_file2))
        # import copy
        # prim_file2 = copy.copy(prim_file)
        # qpylib.log('prim_file2>>>>>>>>>'+str(prim_file2))
        # from werkzeug.datastructures import FileStorage
        # prim_file2 = FileStorage(prim_file)
        # lines = [line for line in prim_file2]
        # qpylib.log('lines>>>>>>>>>' + str(lines))
        # qpylib.log('the magic value>>>>>>>>>>>>>>>>>>' + str(mgic.from_buffer(iobytesiovalue)))
        if 'ASCII text' in mgic.from_buffer(iobytesiovalue):
            if func_re(iobytesiovalue_list[1:-2]):
                pass
            else:
                raise Exception('Enter Valid Certificates')
        else:
            raise Exception('Enter Valid Certificates')

        filename = secure_filename(prim_file.filename)
        if filename.endswith('.zip'):
            zip_file = zipfile.ZipFile(prim_file, "r")
            zip_file.extractall(os.path.join(app.config['PRIMARY_UPLOAD_FOLDER']))
        else:
            if prim_file and allowed_file(prim_file.filename):
                filename = secure_filename(prim_file.filename)
                prim_file.save(os.path.join(app.config['PRIMARY_UPLOAD_FOLDER'], filename))

        try:
            os.remove(app.config['PRIMARY_UPLOAD_FOLDER'] + "/primary.cer")
        except OSError:
            pass
        with open(app.config['PRIMARY_UPLOAD_FOLDER'] + "/primary.cer", "w") as outfile:
            for prim_file in os.listdir(app.config['PRIMARY_UPLOAD_FOLDER']):
                if prim_file.endswith(".cer"):
                    if prim_file.find("CA") == -1:
                        print ""
                    else:
                        with open(os.path.join(app.config['PRIMARY_UPLOAD_FOLDER'], prim_file), 'r') as infile:
                            outfile.write(infile.read())
    except Exception as e:
        qpylib.log("Exception reported : {0}".format(e), level='ERROR')
        dict1 = {"Exception": 'True', "StatusMessage": str(e)}
        failed_response = json.dumps(dict1)
        response_body = BaseResponse(failed_response, 500)

    return response_body

@app.route('/cisco/api/v1/system/settings/test/primary/files', methods=['POST'])
def application_settings_test_primary_files():
    projectservice = Service()
    qpylib.log("Received request to test primary files")
    response = projectservice.testsscertificate("primary")

    return json.dumps(response)


@app.route('/cisco/api/v1/system/settings/update/secondary/files', methods=['POST'])
def application_settings_update_secondary_files():
    projectservice = Service()
    response_body = ""
    qpylib.log("Received request to update system settings files")
    mgic = Magic()
    try:
        _desdir = os.path.join(app.config['SECONDARY_UPLOAD_FOLDER'],
                               'SecondaryCertificates%s' % datetime.datetime.now().strftime("%Y%m%d"))
        if not os.path.exists(_desdir):
            os.makedirs(_desdir)

        for sec_file in os.listdir(app.config['SECONDARY_UPLOAD_FOLDER']):
            if sec_file.endswith(".cer"):
                with open(os.path.join(app.config['SECONDARY_UPLOAD_FOLDER'], sec_file), 'r') as infile:
                    shutil.copy2(os.path.join(app.config['SECONDARY_UPLOAD_FOLDER'], sec_file), _desdir)
        sec_file = request.files['file']
        iobytesiovalue = sec_file.getvalue()
        iobytesiovalue_list = iobytesiovalue.split('\n')
        if 'ASCII text' in mgic.from_buffer(iobytesiovalue):
            if func_re(iobytesiovalue_list[1:-2]):
                pass
            else:
                raise Exception('Enter Valid Certificates')
        else:
            raise Exception('Enter Valid Certificates')
        filename = secure_filename(sec_file.filename)
        if filename.endswith('.zip'):
            zip_file = zipfile.ZipFile(sec_file, "r")
            zip_file.extractall(os.path.join(app.config['SECONDARY_UPLOAD_FOLDER']))
        else:
            if sec_file and allowed_file(sec_file.filename):
                filename = secure_filename(sec_file.filename)
                sec_file.save(os.path.join(app.config['SECONDARY_UPLOAD_FOLDER'], filename))

        try:
            os.remove(app.config['SECONDARY_UPLOAD_FOLDER'] + "/secondary.cer")
        except OSError:
            pass
        with open(app.config['SECONDARY_UPLOAD_FOLDER'] + "/secondary.cer", "w") as outfile:
            for sec_file in os.listdir(app.config['SECONDARY_UPLOAD_FOLDER']):
                if sec_file.endswith(".cer"):
                    if sec_file.find("CA") == -1:
                        print ""
                    else:
                        with open(os.path.join(app.config['SECONDARY_UPLOAD_FOLDER'], sec_file), 'r') as infile:
                            outfile.write(infile.read())
    except Exception as e:
        qpylib.log("Exception reported : {0}".format(e), level='ERROR')
        dict1 = {"Exception": 'True', "StatusMessage": str(e)}
        failed_response = json.dumps(dict1)
        response_body = BaseResponse(failed_response, 500)

    return response_body


@app.route('/cisco/api/v1/system/settings/test/secondary/files', methods=['POST'])
def application_settings_test_secondary_files():
    projectservice = Service()
    qpylib.log("Received request to test secondary files")
    response = projectservice.testsscertificate("secondary")
    return json.dumps(response)


@app.route('/isdataloaded', methods=['POST'])
def isdataloaded():
    projectservice = Service()
    return projectservice.issessiondataloaded()


@app.route('/cisco/api/v1/range_search/call_to_all', methods=['POST'])
def top_most_busy_users_login_name2():
    qpylib.log('Call to all is invoked')
    qradar_response_data = None
    try:
        all_args = request.get_json(silent=True)
        qpylib.log(str(all_args))
        query_param = all_args['query_param']
        qpylib.log('query_param--------------------------->>>>>>>>'+str(query_param))
        tokens = {}

        if 'query_name' not in query_param.keys():
            raise Exception('no query name given')
        else:
            query_name = query_param['query_name']
            if not validator.cummulative_validator(query_name):
                raise Exception('validation error !!!')
            if query_name not in aql_dict.keys():
                raise Exception('wrong query name')
            query_template = aql_dict[query_name]


        if 'filters' not in query_param.keys():
            qpylib.log('filters not present')
            if 'filters' in query_template.encode('utf-8'):
                raise Exception('essential requirement(filters) not present')
        else:
            filters = query_param['filters']
            if not validator.cummulative_validator(filters):
                raise Exception('validation error !!!')
            for i in filters:
                tokens.update({i.encode('utf-8'): filters[i].encode('utf-8')})

        if 'start_date' not in query_param.keys():
            qpylib.log('start_date not present')
            if 'start_date' in query_template.encode('utf-8'):
                raise Exception('essential requirement(start_date) not present')
        else:
            activity_start_date = query_param['start_date']
            if not validator.date_validator(activity_start_date):
                raise Exception('Enter Valid Date')
            else:
                start_date = manageQRadarLatency(activity_start_date, 'start')
                tokens.update({'start_date': start_date})

        if 'end_date' not in query_param.keys():
            qpylib.log('end_date not present')
            if 'end_date' in query_template.encode('utf-8'):
                raise Exception('essential requirement(end_date) not present')
        else:
            activity_end_date = query_param['end_date']
            if not validator.date_validator(activity_end_date):
                raise Exception('Enter Valid Date')
            else:
                end_date = manageQRadarLatency(activity_end_date, 'end')
                tokens.update({'end_date': end_date})
        qhelper = QueryHelper()
        search_client = ArielSearchAPIClient()
        query_expression = qhelper.get_query_expression(query_template, tokens)
        qpylib.log(query_expression)
        qradar_response = search_client.create_search(query_expression)
        qradar_response_data = search_client.create_response(qradar_response)

    except Exception as e:
        qpylib.log("Exception reported : {0}".format(e), level='ERROR')
        if qradar_response_data is None:
            dict1 = {"Exception": 'True', "StatusCode": "1809", "StatusMessage": str(e)}
            responses = json.dumps(dict1)
            from werkzeug.wrappers import BaseResponse
            qradar_response_data = BaseResponse(responses, 500)
    return qradar_response_data


@app.route('/cisco/api/v1/<string:search_id>/status', methods=['GET'])
def get_status(search_id):
    apiInstance = ArielSearchAPIClient()
    search_status_response = apiInstance.get_search(search_id)
    return apiInstance.create_response(search_status_response)


@app.route('/cisco/api/v1/<string:search_id>/result', methods=['POST', 'GET'])
def common_search_result(search_id):
    _result = {}
    search_client = ArielSearchAPIClient()
    all_args = request.get_json(silent=True)
    start_range = None
    end_range = None
    sort_field = None
    sort_dir = None
    filter_field = None
    draw = None
    page_size = None
    filter_value = None
    result_response_type = None
    if 'sort_field' not in all_args.keys():
        qpylib.log('sort_field not present')
    else:
        sort_field = all_args['sort_field']
        if not validator.cummulative_validator(sort_field):
            raise Exception('validation error !!!')

    if 'sort_dir' not in all_args.keys():
        qpylib.log('sort_dir not present')
    else:
        sort_dir = all_args['sort_dir']
        if not validator.cummulative_validator(sort_dir):
            raise Exception('validation error !!!')

    if 'filter_field' not in all_args.keys():
        qpylib.log('filter_field not present')
    else:
        filter_field = all_args['filter_field']
        if not validator.cummulative_validator(filter_field):
            raise Exception('validation error !!!')

    if 'draw' not in all_args.keys():
        qpylib.log('draw not present')
    else:
        draw = all_args['draw']
        if not validator.cummulative_validator(draw):
            raise Exception('validation error !!!')

    if 'page_size' not in all_args.keys():
        qpylib.log('page_size not present')
    else:
        page_size = all_args['page_size']
        if not validator.cummulative_validator(page_size):
            raise Exception('validation error !!!')

    if 'filter_value' not in all_args.keys():
        qpylib.log('filter_value not present')
    else:
        filter_value = all_args['filter_value']
        if not validator.cummulative_validator(filter_value):
            raise Exception('validation error !!!')

    prior_result_response = search_client.get_search_results(search_id, 'application/json')
    the_lenght = search_client.result_response_1(prior_result_response)

    if 'page_number' not in all_args.keys():
        qpylib.log('page_number not present')
    else:
        page_number = all_args['page_number']
        if not validator.cummulative_validator(filter_value):
            raise Exception('validation error !!!')
        else:
            if page_number is None or page_number == 0 or page_number == 1:
                start_range = 0
                end_range = page_size - 1
                qpylib.log("Page number is: " + str(page_number))
            else:
                qpylib.log("Page number is: " + str(page_number))
                end_range = (page_number * page_size) - 1
                start_range = (end_range - page_size) + 1
    result_response = search_client.get_search_results(search_id, 'application/json', start_range, end_range)

    if 'result_response_type' not in all_args.keys():
        qpylib.log('result_response_type not present')
    else:
        result_response_type = all_args['result_response_type']
        if not validator.cummulative_validator(result_response_type):
            raise Exception('validation error !!!')
    if result_response_type == '2d':
        _result = search_client.result_response_pi(result_response)
    if result_response_type == 'grid':
        _result = search_client.result_response(result_response, draw, the_lenght, sort_field, filter_field, filter_value, sort_dir)
        # re = search_client.result_response_pi(result_response)
    return _result


@app.route('/cisco/api/v1/<string:search_id>/export', methods=['GET'])
def export_pxgrid_events(search_id):
    search_client = ArielSearchAPIClient()
    prior_result_response = search_client.get_search_results(search_id, 'application/json')
    the_length = search_client.result_response_1(prior_result_response)
    prior_result = json.loads(prior_result_response.text)

    def generate(prior_result):
        range_start = 0
        chunk = 999  # 1000 at a time
        range_end = chunk if the_length >= chunk else the_length
        # continue_search = True
        stop_export = False
        while (stop_export == False):
            yield search_client.export_completed_search_results(search_id, range_start, range_end)
            if range_end >= the_length:
                stop_export = True
            range_start = range_end + 1
            chunk = chunk if range_end + chunk <= the_length else (the_length - range_end) - 1
            range_end = range_start + chunk

    filename = "pxgrid_" + datetime.datetime.now().strftime("%Y%m%d-%H%M%S") + ".csv"
    h = {'Content-Disposition': "attachment; filename=" + filename}
    return Response(stream_with_context(generate(prior_result)), mimetype='text/csv', headers=h)


@app.route('/cisco/api/v1/range_search/top_risk_alerts')
def top_risk_alerts2():
    qradar_response_data = None
    search_client = ArielSearchAPIClient()
    query_template = aql_dict['range_top_risk_alerts']

    alert_start_date = request.args.get('start_date')
    if not validator.date_validator(alert_start_date):
        raise Exception('Enter Valid Date')

    alert_end_date = request.args.get('end_date')
    if not validator.date_validator(alert_end_date):
        raise Exception('Enter Valid Date')

    start_date = manageQRadarLatency(request.args.get('start_date'), 'start')

    end_date = manageQRadarLatency(request.args.get('end_date'), 'end')

    if validate_inputs(start_date, end_date):
        tokens = {"alert_start_date": alert_start_date, "alert_end_date": alert_end_date, "start_date": start_date, "end_date": end_date}
        qhelper = QueryHelper()
        query_expression = qhelper.get_query_expression(query_template, tokens)
        qradar_response = search_client.create_search(query_expression)
        qradar_response_data = search_client.create_response(qradar_response)
    else:
        qradar_response_data = search_client.get_success_response(messge_ojb)
    return qradar_response_data


@app.route('/cisco/api/v1/show_session_info', methods=['GET'])
def show_session_info():
    context = request.args.get('context')
    if not validator.date_validator(context):
        raise Exception('validation error !!!')

    query_template = aql_dict['session_info']
    tokens = {'context': context}
    qhelper = QueryHelper()
    search_client = ArielSearchAPIClient()

    query_expression = qhelper.get_query_expression(query_template, tokens)
    qpylib.log(query_expression)
    qradar_response = search_client.create_search(query_expression, SEC_token_required=True)

    pxGrid_username = None
    pxGrid_macAddress = None
    pxGrid_postureStatus = None
    pxGrid_endPointProfile = None
    search_id = None
    result_dict = None
    metadata_dict = {}
    qradar_response_data = None
    try:
        qradar_response_data = search_client.create_response(qradar_response)
        responses_dict = json.loads(qradar_response_data.response[0])
        status = str(responses_dict['status'])
        qpylib.log('Metadata status: ' + status)
        if 'COMPLETED' not in status:
            search_id = str(responses_dict['search_id'])
            status_response = search_client.get_search(search_id, SEC_token_required=True)
            status_response = search_client.create_response(status_response)
            responses_dict = json.loads(status_response.response[0])
            status = str(responses_dict['status'])

            while 'COMPLETED' not in status:
                time.sleep(1)
                search_id = str(responses_dict['search_id'])
                status_response = search_client.get_search(search_id, SEC_token_required=True)
                status_response = search_client.create_response(status_response)
                responses_dict = json.loads(status_response.response[0])
                status = str(responses_dict['status'])

        elif 'COMPLETED' in status:
            search_id = str(responses_dict['search_id'])
        qpylib.log('Calling result for search Id: ' + search_id)
        result_response = search_client.get_search_results(search_id, 'application/json', SEC_token_required=True)
        result_dict = json.loads(result_response.text)

        if result_dict is not None and len(result_dict) > 0 and len(result_dict['events']) > 0:
            result_dict = result_dict['events'][0]
            pxGrid_username = result_dict['pxGrid_username']
            pxGrid_macAddress = result_dict['pxGrid_macAddress']
            pxGrid_postureStatus = result_dict['pxGrid_postureStatus']
            pxGrid_endPointProfile = result_dict['pxGrid_endPointProfile']

    except Exception as e:
        qpylib.log("Exception reported : {0}".format(e), level='ERROR')
        if result_dict is None:
            qpylib.log('Inside Exception if loop: ')
            dict1 = {"Exception": 'True', "StatusCode": "1809", "StatusMessage": str(e)}
            responses = json.dumps(dict1)
            from werkzeug.wrappers import BaseResponse
            qradar_response_data = BaseResponse(responses, 500)

    # qpylib.log("Result Dictionary: " + str(result_dict))
    if result_dict is not None and len(result_dict) > 0:
        metadata_dict = {
            'key': 'SessionInfo',
            'label': 'pxGrid Session details:',
            'value': 'Session Information',
            'html': render_template('session_info.html',
                                    username=pxGrid_username,
                                    macaddress=pxGrid_macAddress,
                                    posturestatus=pxGrid_postureStatus,
                                    endpoint=pxGrid_endPointProfile)
        }

    return json.dumps(metadata_dict)
