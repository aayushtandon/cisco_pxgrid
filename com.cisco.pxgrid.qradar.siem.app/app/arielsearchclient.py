__author__ = 'Aujas'

from qpylib import qpylib
import json
import time
from flask import Response
from dbclient import DBClient
import re
from operator import itemgetter
import logging

logging.getLogger().setLevel(logging.INFO)

# from z_test_utils import func1
class ArielSearchAPIClient:
    def __init__(self, version=None):
        #how many number of times the system should attempt to fetch results
        self.threshold = 2
        self.failed_attempts = 3
        self.endpoint_start='api/ariel/searches'
        self.qradar_api_instance = qpylib.strategy()
        #qradar_ip=self.qradar_api_instance.get_console_address()
        self.version= "7.0"
        if version is not None:
            self.version=version
        self.headers={'Accept': 'application/json', 'Version': self.version}
        # self.headers = {"Version": version, 'mime-type': 'application/json', 'Accept': 'application/json',
        #                 'Keep-Alive': 'timeout=5, max=50', 'Authorization': 'Basic base64Encoding'}
        #nothing much to do but we just need to ensure to check for auth and other initial setup.
        dbclient = DBClient()

        self.SEC = dbclient.get_system_setting('qradar_api_user_token')['value']
        self.QRadarCSRF = None

    def sandboxed_call(self,method,endpoint,headers,data=None):
        counter = 0
        while (counter < self.threshold):
            counter = counter + 1

            if data == None:
                response_full = self.qradar_api_instance.REST(method,endpoint,headers)
            else:
                response_full = self.qradar_api_instance.REST(method,endpoint,headers,data)
            if(headers[b'Accept'] != 'application/json'):
                return response_full
            response_json = json.loads(response_full.text)

            if('code' not in response_json):
                #set counter to threshold
                break
            else:
                time.sleep(2)
                qpylib.log("Error during the call, Counter: " + str(counter) + " Full Error:" + response_full.text)
            if counter >= self.threshold:
                qpylib.log("Invalid AQL: exiting")


        return response_full

    #method to create a search
    def create_search(self, query_expression,SEC_token_required=None):
        # sends a POST request to https://<server_ip>/rest/api/ariel/searches
        endpoint= self.endpoint_start
        data = {'query_expression': query_expression}
        #TODO: we need to decide how to handle the error conditions.
        if SEC_token_required is not None and SEC_token_required == True:
            self.headers[b'SEC'] = self.SEC
        search_response=self.sandboxed_call("POST",endpoint,self.headers,data)
        return search_response

    def get_search_results(self, search_id,
                           response_type, range_start=None, range_end=None,SEC_token_required=None):

        headers = self.headers.copy()
        headers[b'Accept'] = response_type
        if SEC_token_required is not None and SEC_token_required == True:
            self.headers[b'SEC'] = self.SEC

        if ((range_start is not None) and (range_end is not None)):
            headers[b'Range'] = ('items=' + str(range_start) + '-' + str(range_end))

        # sends a GET request to
        # https://<server_ip>/rest/api/ariel/searches/<search_id>
        endpoint = self.endpoint_start + "/" + search_id + '/results'   

        # response object body should contain information pertaining to search.
        return self.sandboxed_call('GET',endpoint, headers)

    def get_search(self, search_id,SEC_token_required=None):

        # Sends a GET request to
        # https://<server_ip>/rest/api/ariel/searches/<search_id>
        endpoint = self.endpoint_start + "/" + search_id
        if SEC_token_required is not None and SEC_token_required == True:
            self.headers[b'SEC'] = self.SEC
        return self.sandboxed_call('GET',endpoint, self.headers) 

    def create_response(self,qradar_response):
        response = None
        if qradar_response is not None:
            
            response_dict = json.loads(qradar_response.text)
            response_dict['query_string']=''

            try:
                #qpylib.log('Response here:' + str(response_dict['status']))
                if response_dict['status'] is not None:
                    response = self.get_success_response(response_dict)
                else:
                    response = self.get_server_internal_error_response(response_dict)
            except Exception, e:
                qpylib.log('No status found:')
        else:
            qpylib.log('Qradar response is None:')
        return response

    def result_response(self, qradar_response,draw, the_lenght,sort_field,filter_field,filter_value,sort_dir):
        response = None
        if qradar_response is not None:
            response_dict = json.loads(qradar_response.text)
            value1 = response_dict['events']

            if filter_field is not None and filter_value is not None:
                value1 = filter(lambda value1: value1[filter_field] == filter_value, value1)

            if (sort_dir=='asc'):
                newlist = sorted(value1, key=itemgetter(sort_field))
            else:
                newlist = sorted(value1, key=itemgetter(sort_field), reverse=True)

            del response_dict['events']
            response_dict.update({'data':newlist})
            response_dict.update({'recordsTotal': the_lenght})
            response_dict.update({'recordsFiltered': the_lenght})
            response_dict.update({'draw': draw})
            if response_dict['data'] is not None:
                response = self.get_success_response(response_dict)
            else:
                response = self.get_server_internal_error_response(response_dict)
        else:
            qpylib.log('Qradar response is None:')
        return response
        
    def result_response_pi(self, qradar_response):
        response = None
        if qradar_response is not None:
            response_dict = json.loads(qradar_response.text)
            if response_dict['events'] is not None:
                response = self.get_success_response(response_dict)
            else:
                response = self.get_server_internal_error_response(response_dict)
        else:
            qpylib.log('Qradar response is None:')
        return response

    def result_response_1(self, qradar_response):
        response_dict1 = json.loads(qradar_response.text)
        value1 = response_dict1['events']
        length = len(value1)
        return length 

    def process_dict_response(self,dict_response):
        response = None
        if dict_response is not None:
            if dict_response['events'] is not None:
                response = self.get_success_response(dict_response)
            else:
                response = self.get_server_internal_error_response(dict_response)
        else:
            qpylib.log('Qradar response is None:')
        return response

    def get_success_response(self,data,total_records=None,cache=None):

        resp = Response(response=json.dumps(data, default=self.obj_dict), status=200, mimetype="application/json")

        h = resp.headers
        if data == None:
            data={'cors':"Allowed"}
            
        # prepare headers for CORS in dev mode so we can run the JS from node server
        if qpylib.is_sdk() == True:
            h = resp.headers
            h['Access-Control-Allow-Origin'] = '*'
            h['Access-Control-Allow-Methods'] = 'GET,POST,PUT,OPTIONS'
            h['Access-Control-Allow-Headers'] = 'Origin, X-Requested-With, Content-Type, Accept'
            h['Access-Control-Expose-Headers'] = 'total_records'
        if (cache== None or cache == False): 
            h['Cache-Control'] = 'must-revalidate, no-cache, no-store'
        if not total_records == None:
            h['total_records'] = total_records
            h['Connection']= 'Keep-Alive'
            h['Keep-Alive']= 'timeout=300, max=99'
            resp.headers = h
        return resp

    def get_server_internal_error_response(self,data):
        return Response(response=json.dumps(data),status=500, mimetype="application/json")

    def obj_dict(self,obj):
        return obj.__dict__   

    def export_completed_search_results(self,search_id,range_start,range_end):
        response = self.get_search_results(search_id, 'application/csv', range_start, range_end)
        final_text = ""
        final_text = response.text.replace('\n\n','\n')
        if(range_start > 0):            
            final_text = re.sub('^(.*?)\n','',final_text)            
        return final_text