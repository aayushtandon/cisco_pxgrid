import logging
import logging.handlers
import socket
import time
from qpylib import qpylib
logging.getLogger().setLevel(logging.INFO)
class Facility:
  "Syslog facilities"
  KERN, USER, MAIL, DAEMON, AUTH, SYSLOG, \
  LPR, NEWS, UUCP, CRON, AUTHPRIV, FTP = range(12)

  LOCAL0, LOCAL1, LOCAL2, LOCAL3, \
  LOCAL4, LOCAL5, LOCAL6, LOCAL7 = range(16, 24)

class Level:
  "Syslog levels"
  EMERG, ALERT, CRIT, ERR, \
  WARNING, NOTICE, INFO, DEBUG = range(8)


class CiscoLEEF_Logger:

    def __init__(self, server_name, port, product_vendor, product_name, product_version, facility=Facility.SYSLOG, delimiter='\t'):
        self.DSM_PORT = 514
        self.LEEF_HEADER = '{{date}} {{server_name}} LEEF:2.0|CISCO|QRadarAppForPxGrid|{{product_version}}|{{event_id}}|^| '
        self.server_name = server_name
        self.product_vendor = product_vendor
        self.product_name = product_name
        self.product_version = product_version
        self.delimiter = delimiter
        self.facility = facility
        self.port = 514

        self.socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        #self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        if delimiter not in ['\t', '|', '^']:
            raise ValueError("Delimeter must be '\\t', '|' or '^'")

        self.delimiter = delimiter

        if server_name:
            self.LEEF_HEADER = self.LEEF_HEADER.replace('{{server_name}}', self.server_name)
        else:
            self.LEEF_HEADER = self.LEEF_HEADER.replace('{{server_name}}', '1')

        if product_vendor:
            self.LEEF_HEADER = self.LEEF_HEADER.replace('{{product_version}}', self.product_version)
        else:
            self.LEEF_HEADER = self.LEEF_HEADER.replace('{{product_version}}', '1')


    def create_and_send_events(self, event_name, category, _type, keys, leef_header):
        leef_header_str = leef_header.replace('{{date}}', time.strftime("%b %d %H:%M:%S", time.gmtime())).replace('{{event_id}}', event_name)
        rows =''
        data = keys[_type]
        if len (data) > 0:
            for keys in data:
                payload = ''
                row = ''
                for k, v in keys.items():
                    if k == 'executionSteps':
                        continue
                    key = self.getMappedKey(k,_type)
                    if key == 'src':
                        v = v[0]
                    payload = payload + str(key) + "=" + str(v)+ '\t'
                #logging.info("Payload: {0}".format(str(payload)))
                self.send_events(leef_header_str + payload)

    def send_events(self,message):
        # logging.info ('Sending to QRadar')
        # logging.info(str(message))
        try:
            self.socket.sendto(message.encode('utf-8'), (self.server_name, self.port))
        except Exception as inst:
            qpylib.log("Unexpected error - Unable to write to syslog",level='ERROR')


    def getMappedKey(self,k,_type):
        key = k
        if (_type == 'sessions' or 'failures'):
            if key == 'timestamp':
                key = 'devTime'
            if key == 'macAddress':
                key = 'srcMac'
            if key == 'ipAddresses':
                key = 'src'
            if key == 'userName':
                key = 'usrName'

        # if (_type == 'sessions'):
        #     if key == 'ctsSecurityGroup':
        #         key = 'groupID'
        return key
