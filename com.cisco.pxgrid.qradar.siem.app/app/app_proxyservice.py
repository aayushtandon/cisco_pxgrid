import traceback
from dbclient import DBClient
from responsecreator import ResponseHandler
from arielsearchclient import ArielSearchAPIClient
import logging
from flask import request
from common26 import *
from qpylib import qpylib

sp = None
app_status = None
messge_ojb = {"Message" : "Invalid input"}

logging.getLogger().setLevel(logging.INFO)

class Service(object):

    def __init__(self):

        try:
            payload = None
            dbclient = DBClient()
            settings_dict = dbclient.get_system_settings()
            self.response_handler = ResponseHandler()         
            self.initializeService()
        except :
            qpylib.log("Certificates are unavailable")


    def application_settings_list(self):
        dbclient = DBClient()
        return dbclient.list_system_settings()

    def get_system_setting(self,key):
        dbclient = DBClient()
        return dbclient.get_system_setting(key)

    def enc_key_generator(self):
        key = None
        dbclient = DBClient()
        try:
            from cryptography.fernet import Fernet
            key = Fernet.generate_key()
            dbclient.inserting_encryption_database(key)
        except Exception as expt:
            if 'cryptography.fernet' in str(expt):
                key = ''
        return key

    def fernet_encryption(self,req_key,string_to_be_encrypted):
        token1 = None
        try:
            from cryptography.fernet import Fernet
            f = Fernet(req_key)
            token1 = f.encrypt(string_to_be_encrypted)
        except Exception as error:
            if 'cryptography.fernet' in str(error):
                token1 = string_to_be_encrypted
        return token1

    def fernet_decryption(self,key,token1):
        api_key = None
        try:
            from cryptography.fernet import Fernet
            f = Fernet(key)
            api_key = f.decrypt(str(token1))
        except Exception as mainerror:
            qpylib.log('Error in fernet decryption:- '+str(mainerror), level='ERROR')
        return api_key

    def get_decrypteddata(self, key):
        decry_token = None
        dbclient = DBClient()
        try:
            ency_token = self.get_system_setting(key)['value']
            enc_key_collection = dbclient.retrieving_encryption_database()
            if enc_key_collection == []:
                decry_token = self.get_system_setting(key)['value']
            else:
                enc_key = enc_key_collection[0][1]
                if ency_token!='':
                    decry_token = self.fernet_decryption(str(enc_key), ency_token)

        except Exception as error:
            qpylib.log('Error in get_decrypteddata:- ' + str(error),level='ERROR')
        return decry_token


    def application_settings_update(self,received_json_data):
        dbclient = DBClient()
        params = dict()
        prev_req_key = None
        # fetch old encryption key
        prev_enc_key_collection = dbclient.retrieving_encryption_database()
        if prev_enc_key_collection != []:
            prev_req_key = prev_enc_key_collection[0][1]
        req_key = self.enc_key_generator()

        for item in received_json_data:
            params["id"] = str(item["id"]).strip()
            _key = str(item['key'])
            _value = str(item["value"]).strip()
            if _key in ['qradar_api_user_token']:
                if _value and _value != '**************':
                    #qpylib.log(str(_key)+'................'+'case1')
                    params["value"] = self.fernet_encryption(req_key, str(item["value"]))
                elif _value == '**************':
                    # fetch old encrypted token
                    # decrpyt the token
                    old_token = self.get_system_setting(_key)['value']
                    if prev_req_key!=None:
                        old_token = self.fernet_decryption(str(prev_req_key),str(old_token))
                    params["value"] = self.fernet_encryption(req_key, old_token)
                else:
                    params["value"] = str(item["value"])
            else:
                params["value"] = str(item["value"])
            updatedrecord = dbclient.update_system_setting(params)
            params.clear()

        self.initializeService()
        qpylib.log('Updating system settings')
        dbclient.upsert_active_server_details({'switch_status':'primary'})
        settings = dbclient.list_system_settings()
        primary_status = self.testsscertificate('primary')

        if primary_status != 200:
            qpylib.log('Client activation failed with primary server')
        secondary_status = self.testsscertificate('secondary')
        if secondary_status != 200:
            qpylib.log('Client activation failed with secondary server')

        if primary_status != 200 and secondary_status != 200:
            qpylib.log('Client Activation failed.....')
            return primary_status

        qpylib.log('Client activated...')

        self.create_anc_policy()
        global app_status
        app_status = 'changed'
        return settings


    def initializeService(self):
        dbc = DBClient()
        self.job_execution_interval_in_minutes = dbc.get_system_setting('job_execution_interval_in_minutes')['value']
        self.client_description = 'pxGrid App for QRadar'
        self.client_groups = 'Sessions'
        # self.client_groups = dbc.get_system_setting('client_groups')['value']
        self.primary_pxgrid_server = dbc.get_system_setting('primary_pxgrid_server')['value']
        self.primary_port = int(dbc.get_system_setting('primary_port')['value'])
        self.primary_client_username = dbc.get_system_setting('primary_client_username')['value']
        self.primary_client_cert_key_filepath = dbc.get_system_setting('primary_client_cert_key_filepath')['value']
        self.primary_client_cert_pem_filepath  = dbc.get_system_setting('primary_client_cert_pem_filepath')['value']
        self.primary_ca_cert_filepath  = dbc.get_system_setting('primary_ca_cert_filepath')['value']

        self.secondary_pxgrid_server = dbc.get_system_setting('secondary_pxgrid_server')['value']
        self.secondary_port = int(dbc.get_system_setting('secondary_port')['value'])
        self.secondary_client_username = dbc.get_system_setting('secondary_client_username')['value']
        self.secondary_client_cert_key_filepath = dbc.get_system_setting('secondary_client_cert_key_filepath')['value']
        self.secondary_client_cert_pem_filepath = dbc.get_system_setting('secondary_client_cert_pem_filepath')['value']
        self.secondary_ca_cert_filepath = dbc.get_system_setting('secondary_ca_cert_filepath')['value']

        #self.ca_cert_filepath = 'primary.cer'


    def upsert_active_server_details(self,server,port,available_server_type):
        dbc = DBClient()
        return dbc.upsert_active_server_details({'switch_status':available_server_type})


    def invoke_cisco_api(self, servicename, methodname, payload,server = None):
        rest_response = None
        jsonObj = None

        port = 8910
        client_username = None
        client_cert_key_filepath = None
        client_cert_pem_filepath = None
        ca_cert_filepath = None

        try:
            qpylib.log("Performing service lookup for:"+ servicename)
            _url= '/pxgrid/control/ServiceLookup'

            if server is None:
                server = self.primary_pxgrid_server
            dbc = DBClient()
            active_server_details = dbc.get_active_server_details()
            if(str(active_server_details['switch_status']) == 'primary'):
                server = self.primary_pxgrid_server
                port =self.primary_port
                client_cert_key_filepath =  self.primary_client_cert_key_filepath
                client_cert_pem_filepath = self.primary_client_cert_pem_filepath
                ca_cert_filepath = self.primary_ca_cert_filepath
                client_username = self.primary_client_username
            else:
                server = self.secondary_pxgrid_server
                port = self.secondary_port
                client_cert_key_filepath = self.secondary_client_cert_key_filepath
                client_cert_pem_filepath = self.secondary_client_cert_pem_filepath
                ca_cert_filepath = self.secondary_ca_cert_filepath
                client_username = self.secondary_client_username

            service_response = perform_pxgrid_service_lookup(server,
                                                            port,
                                                            client_cert_key_filepath,
                                                            client_cert_pem_filepath,
                                                            ca_cert_filepath,
                                                            client_username,
                                                            _url,
                                                            servicename)

            if service_response['services'] is None or len(service_response['services']) == 0:
                qpylib.log('No services returned...for : '+servicename)

            qpylib.log("Performing secret retrieval")
            service = service_response['services'][0]
            
            secret = perform_pxGrid_secret_retrieval( server,
                                                        port,
                                                        client_cert_key_filepath,
                                                        client_cert_pem_filepath,
                                                        ca_cert_filepath,
                                                        client_username,
                                                        node_name=service['nodeName'])

            rest_url = self.safeGetUrlsuffix (service, methodname)

            rest_response = send_pxgrid_api_request (server,
                                        port,
                                        client_cert_key_filepath,
                                        client_cert_pem_filepath,
                                        ca_cert_filepath,
                                        client_username,
                                        rest_url,
                                        payload, access_secret=secret)

            if rest_response is not None:
                jsonObj = json.loads(rest_response)

        except Exception as inst:
            qpylib.log("Exception reported from invoke_cisco_api method: {0}".format(str(type(inst))),level='ERROR')
            qpylib.log("Exception reported from invoke_cisco_api method: {0}".format(str(inst)),level='ERROR')
            qpylib.log(traceback.format_exc())
        return jsonObj


    def invoke_cisco_ws_api(self, servicename, methodname, payload, server = None):
        status = 'stopped'
        port = 8910
        client_username = None
        client_cert_key_filepath = None
        client_cert_pem_filepath = None
        ca_cert_filepath = None
        try:
            qpylib.log("Performing service lookup for:"+ servicename)
            _url= '/pxgrid/control/ServiceLookup'

            dbc = DBClient()
            active_server_details = dbc.get_active_server_details()
            if (str(active_server_details['switch_status']) == 'primary'):
                server = self.primary_pxgrid_server
                port = self.primary_port
                client_cert_key_filepath = self.primary_client_cert_key_filepath
                client_cert_pem_filepath = self.primary_client_cert_pem_filepath
                ca_cert_filepath = self.primary_ca_cert_filepath
                client_username = self.primary_client_username
            else:
                server = self.secondary_pxgrid_server
                port = self.secondary_port
                client_cert_key_filepath = self.secondary_client_cert_key_filepath
                client_cert_pem_filepath = self.secondary_client_cert_pem_filepath
                ca_cert_filepath = self.secondary_ca_cert_filepath
                client_username = self.secondary_client_username

            service_response = perform_pxgrid_service_lookup(server,
                                                            port,
                                                            client_cert_key_filepath,
                                                            client_cert_pem_filepath,
                                                            ca_cert_filepath,
                                                            client_username,
                                                            _url,
                                                            servicename)

            if service_response['services'] is None or len(service_response['services']) == 0:
                print('No services returned...for : '+servicename)

            qpylib.log("Performing sceret retrieval")

            service = service_response['services'][0]
            
            secret = perform_pxGrid_secret_retrieval( server,
                                                        port,
                                                        client_cert_key_filepath,
                                                        client_cert_pem_filepath,
                                                        ca_cert_filepath,
                                                        client_username,
                                                        node_name=service['nodeName'])

            ws_url = self.safeGetWsUrlsuffix (service)
            #Added /topic/com.cisco.ise.mdm.endpoint to get offline mdm data which is not part of Session.
            topic_list = ['/topic/com.cisco.ise.session','/topic/com.cisco.ise.radius.failure','/topic/com.cisco.ise.config.anc.status','/topic/com.cisco.ise.mdm.endpoint']
            status = send_pxgrid_ws_request (ws_url,
                                    topic_list,
                                    client_cert_key_filepath,
                                    client_cert_pem_filepath,
                                    ca_cert_filepath,
                                    client_username,
                                    access_secret=secret)

        except Exception as inst:
            qpylib.log("Exception reported from invoke_cisco_ws_api method: {0}".format(str(type(inst))),level='ERROR')
            qpylib.log("Exception reported from invoke_cisco_ws_api method: {0}".format(str(inst)),level='ERROR')
            raise inst
        
        return status


    def safeGetUrlsuffix(self, service, methodname):
        url = None
        try:
            url = service['properties']['restBaseUrl'] + '/'+methodname
        except Exception as inst:
            url = service['properties']['restBaseURL'] + '/'+methodname
            pass
        return url


    def safeGetWsUrlsuffix(self, service):
        url = None
        try:       
            url = service['properties']['wsUrl']
        except Exception as inst:
            url = service['properties']['wsURL']
            pass
        return url


    def create_anc_policy(self):
        self.initializeService()

        service_name='com.cisco.ise.config.anc'
        endpoint = 'createPolicy'
        
        qpylib.log ('Creating ANC Action pxGridQRadarQuarantine --> QUARANTINE')
        payload = json.dumps({"name":"pxGridQRadarQuarantine","actions":["QUARANTINE"]})
        response = self.invoke_cisco_api(service_name, endpoint, payload)
        qpylib.log ('Primary Server Response: '+ str(response))

        qpylib.log ('Creating ANC Action pxGridQRadarShutDown -> SHUT_DOWN')
        payload = json.dumps({"name":"pxGridQRadarShutDown","actions":["SHUT_DOWN"]})
        response = self.invoke_cisco_api(service_name, endpoint, payload)
        qpylib.log ('Primary Server Response: '+ str(response))

        qpylib.log ('Creating ANC Action pxGridQRadarPortBounce - PORT_BOUNCE')
        payload = json.dumps({"name":"pxGridQRadarPortBounce","actions":["PORT_BOUNCE"]})
        response = self.invoke_cisco_api(service_name, endpoint, payload)
        qpylib.log ('Primary Server Response: '+ str(response))


    def testsscertificate(self, environment,msg='Testing'):
        self.initializeService()
        port = 8910
        client_username = None
        client_cert_key_filepath = None
        client_cert_pem_filepath = None
        ca_cert_filepath = None
        if(environment == 'primary'):
            server = self.primary_pxgrid_server
            if not server:
                return 800
            port = self.primary_port
            if not port:
                return 801
            client_cert_key_filepath = self.primary_client_cert_key_filepath
            if not client_cert_key_filepath:
                return 802
            client_cert_pem_filepath = self.primary_client_cert_pem_filepath
            if not client_cert_pem_filepath:
                return 803
            ca_cert_filepath = self.primary_ca_cert_filepath
            if not ca_cert_filepath:
                return 804
            client_username = self.primary_client_username
            if not client_username:
                return 805
        else:
            server = self.secondary_pxgrid_server
            if not server:
                return 900
            port = self.secondary_port
            if not port:
                return 901
            client_cert_key_filepath = self.secondary_client_cert_key_filepath
            if not client_cert_key_filepath:
                return 902
            client_cert_pem_filepath = self.secondary_client_cert_pem_filepath
            if not client_cert_pem_filepath:
                return 903
            ca_cert_filepath = self.secondary_ca_cert_filepath
            if not ca_cert_filepath:
                return 904
            client_username = self.secondary_client_username
            if not client_username:
                return 905
        return test_ssl_http_connection(server, port, client_cert_key_filepath, client_cert_pem_filepath, ca_cert_filepath,client_username, environment,msg)


    def testws(self):
        self.initializeService()
        dbc = DBClient()
        port = 8910
        client_username = None
        client_cert_key_filepath = None
        client_cert_pem_filepath = None
        ca_cert_filepath = None
        if (dbc.get_active_server_details()['switch_status'] == 'primary'):
            server = self.primary_pxgrid_server
            port = self.primary_port
            client_cert_key_filepath = self.primary_client_cert_key_filepath
            client_cert_pem_filepath = self.primary_client_cert_pem_filepath
            ca_cert_filepath = self.primary_ca_cert_filepath
            client_username = self.primary_client_username
        else:
            server = self.secondary_pxgrid_server
            port = self.secondary_port
            client_cert_key_filepath = self.secondary_client_cert_key_filepath
            client_cert_pem_filepath = self.secondary_client_cert_pem_filepath
            ca_cert_filepath = self.secondary_ca_cert_filepath
            client_username = self.secondary_client_username
        return test_ws_connection(server, port, client_cert_key_filepath, client_cert_pem_filepath, ca_cert_filepath, client_username)


    def swapservers(self):
        dbc = DBClient()
        qpylib.log('Swapping servers..........')
        active_server_status = None
        active_server = str(dbc.get_active_server_details()['switch_status'])
        if 'primary' in active_server :
            qpylib.log('Switching to secondary server')
            active_server_status = 'secondary'
        elif 'secondary' in active_server:
            qpylib.log('Switching to primary server')
            active_server_status = 'primary'
        params = {'switch_status':active_server_status}
        return dbc.upsert_active_server_details(params)

    def issessiondataloaded(self):
        data = None
        try:
            search_status_response = None
            search_client = ArielSearchAPIClient()
            sql_expr = "SELECT starttime FROM events WHERE LOGSOURCENAME(logsourceid) = 'pxGrid' LIMIT 1 LAST 30 DAYS"
            qradar_response = search_client.create_search(sql_expr)
            response_dict = json.loads(qradar_response.text)
            search_id = response_dict['search_id']
            searchinprogress = True
            while searchinprogress:
                search_status_response = search_client.get_search(search_id)
                response_status_dict = json.loads(search_status_response.text)
                if(response_status_dict['status'] == 'COMPLETED'):
                    searchinprogress = False
                else:
                    time.sleep(10)
            result_response = search_client.get_search_results(search_id, 'application/json')
            result_dict = json.loads(result_response.text)
            data = result_dict['events']
        except Exception as exp:
            qpylib.log('Unable to get events data '+ str(exp))
        return data




