__author__ = 'Aujas'

import os
import sys
import json
import re
import threading
import logging
import time
import traceback
from qpylib import qpylib
from flask import Flask, send_from_directory, render_template, request,current_app
from datetime import datetime
from common26 import *
from CiscoLEEFormater import *
from apscheduler.scheduler import Scheduler,JobStoreEvent, JobEvent
from dbclient import DBClient
from app_proxyservice import Service
from flask_wtf.csrf import CSRFProtect, CSRFError
app = Flask(__name__)
from app import views
logging.getLogger().setLevel(logging.INFO)
# Create log here to prevent race condition when importing views
qpylib.register_jsonld_endpoints()
qradar_api_instance = qpylib.strategy()
logging.basicConfig()
dbclient = DBClient()
appsettingsCache = dict()
scheduler = Scheduler()

qpylib.create_log()
qradar_api_instance = qpylib.strategy()
time_interval = 3
job_status = 'stopped'

#logging.getLogger().setLevel(logging.INFO)
logger = logging.getLogger()
logger.propagate = False
qpylib_logger = logging.getLogger('com.ibm.applicationLogger')
qpylib_logger.propagate = False


app_secret_key = os.urandom(24)
app.secret_key = app_secret_key

# CSRF configuration
csrf = CSRFProtect(app)
csrf.init_app(app)


# Every relevant request will be checked for the CSRF header
@app.before_request
def check_csrf():
    csrf.protect()


@app.errorhandler(CSRFError)
def handle_csrf_error(e):
    return render_template('csrf_error.html', reasons=e.description), 400


@app.route('/getappid', methods=['GET'])
@app.route('/get_app_id', methods=['GET'])
@app.route('/get_application_id', methods=['GET'])
@app.route('/cisco/api/v1/getappid', methods=['GET'])
@app.route('/cisco/api/v1/get_app_id', methods=['GET'])
@app.route('/cisco/api/v1/get_application_id', methods=['GET'])
def get_application_id():
    context = request.args.get("context")
    if context is None or context == '':
        context = 'null'
    qpylib.log("Setting the results to: " + context)
    secret_key = render_template('render.html')

    return json.dumps({"applicationId": qpylib.get_app_id(), "context": context, "csrf_token": str(secret_key).strip()})


@app.route('/debug')
def debug():
    return send_from_directory('/store/log/', 'app.log')


@app.route('/debug_view')
def debug_view():
    debug_content = open('/store/log/app.log').read()
    return render_template('debug.html', debug_content=debug_content)


@app.route('/resources/<path:filename>')
def send_file(filename):
    qpylib.log(" >>> route resources >>>")
    qpylib.log(" filename=" + filename)
    qpylib.log(" app.static_folder=" + app.static_folder)
    qpylib.log(" full file path =" + app.static_folder + '/resources/'+filename)
    return send_from_directory(app.static_folder, 'resources/'+filename)


@app.route('/log_level', methods=['POST'])
def log_level():
    level = request.form['level'].upper()
    levels = ['INFO', 'DEBUG', 'ERROR', 'WARNING', 'CRITICAL']

    if any(level in s for s in levels):
        qpylib.set_log_level(request.form['level']) 
    else:
        return 'level parameter missing or unsupported - ' + str(levels), 42
    return 'log level set to ' + level


# register the new q_url_for() method for use with Jinja2 templates
app.add_template_global(qpylib.q_url_for, 'q_url_for')


def job_listener(event):
    if isinstance(event, JobEvent):
        qpylib.log('Job Type is: JobEvent')

    elif isinstance(event, JobStoreEvent):
        qpylib.log('Job Type is: JobStoreEvent')
    elif isinstance(event, Exception):
        qpylib.log('Trackback: {0}'.format(traceback.format_exc()))
        try:
            qpylib.log("Job exception is {0}".format(event.message))
            logging.getLogger().setLevel(logging.ERROR)
            qpylib.log('Job type {0}'.format(str(type(event))))
            qpylib.log('Job Exception {0}'.format(str(event)))
            logging.getLogger().setLevel(logging.INFO)
        except Exception:
            pass
    else:
        qpylib.log('Job Type: {0}'.format(str(type(event))))
        # logging.info('Job: {}'.format(str(event)))


qpylib.log('Scheduler Status: {0}'.format(str(scheduler.running)))
scheduler.add_listener(job_listener)


def rescheduleJobInterval(time_in_min):
    global scheduler
    jobs = scheduler.get_jobs()
    qpylib.log('Total number of Jobs: {0}'.format(str(len(jobs))))

    for job in jobs:
        qpylib.log('Unscheduling job:' + str(job))

        try:
            scheduler.unschedule_job(job)
            # scheduler._remove_job(job)
        except Exception as e:
            qpylib.log('Exception while unscheduling the job: {0}'.format(e.message))
    reschedule_interval = '*/' + str(time_in_min)
    qpylib.log('Rescheduling the job.....')
    scheduler.add_cron_job(startapp, day_of_week='*', hour='*', minute=reschedule_interval, misfire_grace_time=30, coalesce=True)


qpylib.log("Python version: "+str(sys.version_info[0]) + "."+str(sys.version_info[1])+"."+str(sys.version_info[2]))


def checkforcertavailablity():
    dbc = DBClient()
    active_server_details = dbc.get_active_server_details()

    if str(active_server_details['switch_status']) == 'primary':
        cert_exists = os.path.isfile('store/primary/primary.cer')
    else:
        cert_exists = os.path.isfile('store/secondary/secondary.cer')
    cert_exists = cert_exists or False
    return cert_exists


def checkandswapservers(ps):

    # start_time = time.time()
    # current_datetime = datetime.utcnow()
    # current_date = current_datetime.strftime("%Y-%m-%d")
    payload = None
    if ps is None:
        ps = Service()

    qpylib.log("Primary Server: {0}".format(ps.primary_pxgrid_server))
    qpylib.log("Secondary Server: {0}".format(ps.secondary_pxgrid_server))
    payload = json.dumps({'description': 'pXGrid App for QRadar', 'groups': 'Session'})
    activation_status = None
    port = 8910
    client_username = None
    client_cert_key_filepath = None
    client_cert_pem_filepath = None
    ca_cert_filepath = None
    dbc = DBClient()
    active_server_details = dbc.get_active_server_details()
    qpylib.log("Current Active Server: " + str(active_server_details['switch_status']))
    if str(active_server_details['switch_status']) == 'primary':
        server = ps.primary_pxgrid_server
        port = ps.primary_port
        client_cert_key_filepath = ps.primary_client_cert_key_filepath
        client_cert_pem_filepath = ps.primary_client_cert_pem_filepath
        ca_cert_filepath = ps.primary_ca_cert_filepath
        client_username = ps.primary_client_username
    else:
        server = ps.secondary_pxgrid_server
        port = ps.secondary_port
        client_cert_key_filepath = ps.secondary_client_cert_key_filepath
        client_cert_pem_filepath = ps.secondary_client_cert_pem_filepath
        ca_cert_filepath = ps.secondary_ca_cert_filepath
        client_username = ps.secondary_client_username

    try:
        activation_status = activatePxGridAccount(server,
                                                  port,
                                                  client_cert_key_filepath,
                                                  client_cert_pem_filepath,
                                                  ca_cert_filepath,
                                                  payload,
                                                  client_username,
                                                  '',
                                                  '/pxgrid/control/AccountActivate')

        qpylib.log('Account Activation Status: {0}'.format(str(activation_status)))
    except Exception as e:
        logging.getLogger().setLevel(logging.ERROR)
        qpylib.log("Exception reported from activatePxGridAccount method: {0}".format(type(e)))
        qpylib.log("Exception reported from activatePxGridAccount method: {0}".format(e))
    logging.getLogger().setLevel(logging.INFO)
    if activation_status is None or activation_status != 200:
        qpylib.log('Waiting and will re-try with another server after two minute')
        time.sleep(60)
        ps.swapservers()
        qpylib.log('Re-trying with another server')
        checkandswapservers(Service())

    return activation_status


def subscribefromwebsocket(ps):
    global job_status
    try:
        ps_status = ps.invoke_cisco_ws_api('com.cisco.ise.pubsub', '/topic/com.cisco.ise.session', json.dumps({}))
        job_status = ps_status
    except RuntimeError as rte:
        logging.getLogger().setLevel(logging.ERROR)
        job_status = 'stopped'
        qpylib.log("RuntimeError reported from subscribefromwebsocket method: {0}".format(type(rte)))
        qpylib.log("RuntimeError reported from subscribefromwebsocket method: {0}".format(str(rte)))
    except Exception as inst:
        logging.getLogger().setLevel(logging.ERROR)
        job_status = 'stopped'
        qpylib.log("Exception reported from subscribefromwebsocket method: {0}".format(type(inst)))
        qpylib.log("Exception reported from subscribefromwebsocket method: {0}".format(inst))
    except:
        logging.getLogger().setLevel(logging.ERROR)
        job_status = 'stopped'
        qpylib.log("Unexpected error:" + str(sys.exc_info()[0]))
    finally:
        logging.getLogger().setLevel(logging.INFO)
        qpylib.log("Called subscribefromwebsocket:")
    return job_status


def bulkget(ps):
    global job_status
    dbc = DBClient()
    port = 8910
    client_username = None
    client_cert_key_filepath = None
    client_cert_pem_filepath = None
    ca_cert_filepath = None
    qpylib.log("Called bulkget:"+str(dbc.get_active_server_details()))
    if str((dbc.get_active_server_details()['switch_status']) == 'primary'):
        server = ps.primary_pxgrid_server
        port = ps.primary_port
        client_cert_key_filepath = ps.primary_client_cert_key_filepath
        client_cert_pem_filepath = ps.primary_client_cert_pem_filepath
        ca_cert_filepath = ps.primary_ca_cert_filepath
        client_username = ps.primary_client_username
    else:
        server = ps.secondary_pxgrid_server
        port = ps.secondary_port
        client_cert_key_filepath = ps.secondary_client_cert_key_filepath
        client_cert_pem_filepath = ps.secondary_client_cert_pem_filepath
        ca_cert_filepath = ps.secondary_ca_cert_filepath
        client_username = ps.secondary_client_username
    try:
        start_time = time.time()
        # current_datetime = datetime.utcnow()
        # current_date = current_datetime.strftime("%Y-%m-%d")
        # activation_status = checkandswapservers(ps)
        ps = Service()



        lookup_response = perform_pxgrid_service_lookup(server,
                                port,
                                client_cert_key_filepath,
                                client_cert_pem_filepath,
                                ca_cert_filepath,
                                client_username,
                                '/pxgrid/control/ServiceLookup', 
                                'com.cisco.ise.session')

        param = 'no'

        if param is None or len(param) == 0 or 'quit' in param:
            return

        if lookup_response['services'] is None or len(lookup_response['services']) == 0:
            qpylib.log('No services returned...')
            return
        
        for service in lookup_response['services']:
            secret = perform_pxGrid_secret_retrieval(server,
                                                    port,
                                                    client_cert_key_filepath,
                                                    client_cert_pem_filepath,
                                                    ca_cert_filepath,
                                                    client_username,
                                                    node_name=service['nodeName'])

            payload = "{}"
            if 'no' not in param:
                timestamp = time.time()

                param = str(datetime.fromtimestamp(timestamp).isoformat())
                qpylib.log('Timestamp format:'+str(param))
                payload = json.dumps({"startTimestamp": param})
                qpylib.log(str(payload))
                qpylib.log('Getting sessions by timestamp (' + param + ')...')

            response = send_pxgrid_api_request(server,
                                                port,
                                                client_cert_key_filepath,
                                                client_cert_pem_filepath,
                                                ca_cert_filepath,
                                                client_username,
                                                service['properties']['restBaseUrl'] + '/getSessions', 
                                                payload,
                                                access_secret=secret)

            service_name = 'com.cisco.ise.radius'
            payload=json.dumps({})
            radius_response = ps.invoke_cisco_api(service_name, 'getFailures', payload)
            # server_ip = ps.primary_pxgrid_server
            qradar_api_instance1 = qpylib.strategy()
            server_ip = qradar_api_instance1.get_console_address()
            qpylib.log('SysLog Header IP:'+str(server_ip))
            leef = CiscoLEEF_Logger(server_ip,
                                    514, 
                                    'CISCO', 
                                    'QRadarAppForPxGrid', 
                                    '1.0',
                                    facility=Facility.SYSLOG, 
                                    delimiter="|")

            items = json.loads(response)
            
            leef.create_and_send_events('User Sessions', 'Sessions', 'sessions', items, leef.LEEF_HEADER)
            leef.create_and_send_events('Radius Failure', 'Radius', 'failures', radius_response, leef.LEEF_HEADER)

        end_time = time.time()
        qpylib.log('Total time taken for completing bulk load of one time data: ' + str((end_time - start_time)))
        job_status = 'started'

    except RuntimeError as rte:
        logging.getLogger().setLevel(logging.ERROR)
        job_status = 'stopped'
        qpylib.log("RuntimeError reported from bulkget method: {0}".format(type(rte)))
        qpylib.log("RuntimeError reported from bulkget method: {0}".format(rte))
    except Exception as inst:
        logging.getLogger().setLevel(logging.ERROR)
        job_status = 'stopped'
        qpylib.log("Exception reported from bulkget method: {0}".format(type(inst)))
        qpylib.log("Exception reported from bulkget method: {0}".format(inst))
    except:
        logging.getLogger().setLevel(logging.INFO)
        job_status = 'stopped'
        qpylib.log("Unexpected error:" + str(sys.exc_info()[0]))
    return job_status


def startapp():
    global job_status
    global time_interval
    global scheduler
    ps = Service()
    dbc = DBClient()

    activation_status = 0
    try:
        time_interval = ps.job_execution_interval_in_minutes

        job_status = dbc.getjobststus()['current_job_status']
        active_server = str(dbc.get_active_server_details()['switch_status'])
        test_status = ps.testsscertificate(active_server, 'Checking')

        qpylib.log("{0} server connection status is {1}".format(active_server, str(test_status)))
        if test_status != 200:
            ps.swapservers()
            rescheduleJobInterval(time_interval)
            return

        events = ps.issessiondataloaded()
        try:
            if events is not None and len(events) == 0:
                qpylib.log("Calling bulk load..........")
                bulk_job_status = bulkget(ps)
                if bulk_job_status == 'stopped':
                    qpylib.log("Bulk loading of events failed. This is a one time activity at first execution.")

        except Exception as exp:
            qpylib.log(traceback.format_exc())
            qpylib.log("Exception in bulk loading of events. This is a one time activity at first execution.")
            qpylib.log("Bulk loading failed: {0}".format(exp.message))

        finally:
            time_interval = ps.job_execution_interval_in_minutes

        try:
            job_status = 'started'
            job_status = subscribefromwebsocket(ps)
        except Exception as exp:
            qpylib.log("Subscribe from websocket failed: {0}".format(exp.message))
            qpylib.log(traceback.format_exc())
            dbc.updatejob_status({'current_job_status': 'exception'})
            job_status = 'stopped'
            # raise exp
        finally:
            time_interval = ps.job_execution_interval_in_minutes

    except Exception as _expt:
        qpylib.log("Exception: from startapp method: {0}".format(str(_expt)))
        time_interval = ps.job_execution_interval_in_minutes
        dbc.updatejob_status({'current_job_status': 'exception'})
        qpylib.log(traceback.format_exc())
        pass


# scheduler.add_interval_job(startapp, minutes=int(time_interval), misfire_grace_time=60, coalesce=True)
interval_schedule = '*/'+str(time_interval)
qpylib.log("Starting a new APScheduler cron job...")
scheduler.add_cron_job(startapp, day_of_week='*', hour='*', minute=interval_schedule, misfire_grace_time=30, coalesce=True)
scheduler.start()
