def ip_validator(ip_obj):
    import ipaddress
    try:
        ip_val = ipaddress.ip_address(ip_obj)
        return True
    except Exception as error:
        return False

def cummulative_validator(hostname):
    import re
    if len(hostname) > 255:
        return False
    if hostname[-1] == ".":
        hostname = hostname[:-1]
    allowed = re.compile("(?!-)[&#@+=:/A-Z\s\w\d-]{1,100}(?<!-)$", re.IGNORECASE)
    return all(allowed.match(x) for x in hostname.split("."))

def date_validator(date_obj):
    flag = 0
    date_list = []
    date_obj = date_obj.split(' ')[0]
    if '/' in date_obj:
        date_list = date_obj.split('/')
    elif '-' in date_obj:
        date_list = date_obj.split('-')
    for i in date_list:
        if cummulative_validator(i):
            continue
        else:
            flag = 1
            break
    if flag == 1:
        return False
    else:
        return True

def sha_validator(sha_obj):
    if len(sha_obj)==16 and cummulative_validator(sha_obj):
        return True
    else:
        return False

def mac_validator(mac_add):
    import re
    if re.match("[0-9a-f]{2}([-:])[0-9a-f]{2}(\\1[0-9a-f]{2}){4}$", mac_add.lower()):
        return True
    else:
        return False