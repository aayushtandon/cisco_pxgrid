# -*- coding: utf-8 -*-
import os
import io
import json
import time
import base64
import urllib2
import socket, ssl, pprint
import httplib
import traceback
import logging
from sys import platform
from qpylib import qpylib
from ws4py import websocket, client
from ws4py.client.threadedclient import WebSocketClient
from CiscoLEEFormater import *
from dbclient import DBClient

status = 'stopped'
logging.getLogger().setLevel(logging.INFO)

def send_rest_request(hostname, url_suffix, context, username, payload, content_type='json', access_secret=''):
    address = url_suffix
    if 'https' not in address:
        address = 'https://' + hostname + ':8910/pxgrid/control/' + url_suffix
    qpylib.log(address)
    b64 = base64.b64encode((username + ':' + access_secret).encode()).decode()
    data = None
    if payload is not None:
        data = str.encode(payload)

    rest_request = urllib2.Request(url=address, data=data)
    rest_request.add_header('Content-Type', 'application/' + content_type)
    rest_request.add_header('Accept', 'application/' + content_type)
    rest_request.add_header('Authorization', 'Basic ' + b64)
    try:
        rest_response = urllib2.urlopen(rest_request, context=context)
        decoded_resposne = rest_response.read().decode()
        return decoded_resposne
    except urllib2.HTTPError as err:
        qpylib.log("HTTPError from while retrieving ")
        qpylib.log(err)



def send_pxgrid_api_request(host, 
                            port, 
                            key_file, 
                            cert_file, 
                            cacerts, 
                            user_name, 
                            _url, payload, 
                            access_secret=''):

    qpylib.log('Sending request to pxGrid')
    dbc = DBClient()
    active_server = str(dbc.get_active_server_details()['switch_status'])
    cert_path = 'store/' + active_server + '/'
    # cert_path='store/primary/'
    # ssl.SSLContext
    httpsConn = None
    try:
        httpsConn = httplib.HTTPSConnection(host, port)
        sock = socket.create_connection((host, port))
        httpsConn.sock = ssl.wrap_socket(sock, 
                                        keyfile=cert_path+key_file, 
                                        certfile=cert_path+cert_file, 
                                        server_side=False, 
                                        cert_reqs=ssl.CERT_REQUIRED, 
                                        ssl_version=ssl.PROTOCOL_SSLv23, 
                                        ca_certs=cert_path+cacerts,
                                        do_handshake_on_connect=True, 
                                        suppress_ragged_eofs=True)

        b64 = base64.b64encode((user_name + ':' + access_secret).encode()).decode()

        httpsConn.request(method="POST", 
                            url=_url, 
                            headers= {"Content-type": "application/json", 'Connection': 'keep-alive', 'Authorization': 'Basic ' + b64},
                            body = payload)

        res = httpsConn.getresponse()
        json_response = res.read()

    except ssl.SSLError as e:
        qpylib.log("Trying SSLv23.", str(e))
        raise e
    except Exception as e:
        qpylib.log(traceback.format_exc())
        raise e
    finally:
        if httpsConn:
            httpsConn.close()

    str_respose = str(json_response)

    if 'secret' not in str_respose:
        qpylib.log('Response: ' + str(str_respose) + '\n')
    
    return json_response


def send_pxgrid_ws_request1(host,
                            port,
                            key_file,
                            cert_file,
                            cacerts,
                            user_name,
                            _url, payload,
                            access_secret=''):

    # logging.info('Sending request to pxGrid')
    json_response = None
    dbc = DBClient()
    active_server = str(dbc.get_active_server_details()['switch_status'])
    cert_path = 'store/' + active_server + '/'
    httpsConn = None
    try:
        httpsConn = httplib.HTTPSConnection(host, port)
        sock = socket.create_connection((host, port))
        httpsConn.sock = ssl.wrap_socket(sock, 
                                        keyfile=cert_path+key_file, 
                                        certfile=cert_path+cert_file, 
                                        server_side=False, 
                                        cert_reqs=ssl.CERT_REQUIRED, 
                                        ssl_version=ssl.PROTOCOL_SSLv23,
                                        ca_certs=cert_path+cacerts,
                                        do_handshake_on_connect=True, 
                                        suppress_ragged_eofs=True)

        b64 = base64.b64encode((user_name + ':' + access_secret).encode()).decode()

        httpsConn.request(method="POST", 
                            url=_url, 
                            headers= {"Content-type": "application/json", 'Connection': 'keep-alive','Authorization': 'Basic ' + b64},
                            body = payload)

        res = httpsConn.getresponse()
        json_response = res.read()
    except ssl.SSLError, e:
        qpylib.log("Trying SSLv23.", str(e))
    except Exception, e:
        qpylib.log('Exception while calling send_pxgrid_ws_request1...')
        qpylib.log(traceback.format_exc())
    finally:
        if httpsConn:
            httpsConn.close()

    str_response = str(json_response)

    if 'secret' not in str_response:
        qpylib.log('Response: ' + str_response[0:10]+'.....' + '\n')

    return json_response


def perform_account_activate(hostname, context, username, description, groups):
    qpylib.log('Activating client account...')
    account_state = ''
    json_response = None
    while 'ENABLED' not in account_state:
        payload = json.dumps({'description': description, 'groups': groups})
        raw_response = send_rest_request(hostname, 'AccountActivate', context, username, payload)
        if raw_response is not None:
            try:
                qpylib.log('Raw Response: ' + str(raw_response))
                json_response = json.loads(raw_response)
                account_state = json_response['accountState']
                if 'PENDING' in account_state:
                    qpylib.log('Server approval is still pending. Waiting 10 sec before retrying...')
                    time.sleep(10)
                qpylib.log('Client account has been activated! Server pxGrid version: ' + json_response['version'] + '\n')
            except Exception as inst:
                print (type(inst))


def perform_service_lookup(hostname, context, service_name, username):
    qpylib.log('Performing service look up to retrieve Websocket URL...')
    payload = json.dumps({'name': service_name})
    raw_response = send_rest_request(hostname, 'ServiceLookup', context, username, payload)
    json_response = json.loads(raw_response)
    return json_response


def perform_pxgrid_service_lookup(host, port, key_file, cert_file, cacerts, user_name, _url, service_name):
    qpylib.log('Performing pxGrid service look up to retrieve Websocket URL...')
    dbc = DBClient()
    json_response = None
    active_server = str(dbc.get_active_server_details()['switch_status'])
    cert_path = 'store/' + active_server + '/'
    https_conn = None
    try:
        https_conn = httplib.HTTPSConnection(host, port)
        sock = socket.create_connection((host, port))
        qpylib.log("Server {0}".format(str(host)))
        qpylib.log("Port {0}".format(str(port)))
        #qpylib.log("Client Key {0}".format(str(key_file)))
        #qpylib.log("Client Certificate {0}".format(str(cert_file)))
        #qpylib.log("Root CA Certificate {0}".format(str(cacerts)))
        qpylib.log("Client Name {0}".format(str(user_name)))
        qpylib.log("URL {0}".format(str(_url)))
        qpylib.log("Service Name {0}".format(str(service_name)))

        https_conn.sock = ssl.wrap_socket(sock,
                                        keyfile=cert_path+key_file, 
                                        certfile=cert_path+cert_file, 
                                        server_side=False, 
                                        cert_reqs=ssl.CERT_REQUIRED, 
                                        ssl_version=ssl.PROTOCOL_SSLv23, 
                                        ca_certs=cert_path+cacerts,
                                        do_handshake_on_connect=True, 
                                        suppress_ragged_eofs=True)
        payload = json.dumps({'name': service_name})
        b64 = base64.b64encode((user_name + ':' + '').encode()).decode()
        https_conn.request(method="POST",
                            url=_url,
                            headers={"Content-type": "application/json", 'Connection': 'keep-alive','Authorization': 'Basic ' + b64},
                            body=payload)
        res = https_conn.getresponse()
        json_response = res.read()
    except ssl.SSLError as sslerr:
        qpylib.log("Trying with SSLv23.", str(sslerr))
        raise sslerr
    except Exception, e:
        qpylib.log('Exception while calling perform_pxgrid_service_lookup...')
        qpylib.log(traceback.format_exc())
        raise e
    finally:
        if https_conn:
            https_conn.close()

    return json.loads(json_response)


def perform_ws_service_lookup(hostname, context, service_name, username):
    json_response = perform_service_lookup(hostname, context, service_name, username)
    service = json_response['services'][0]  # should be cycling through services and choosing
    return service['nodeName'], service['properties']['wsUrl']


def perform_access_secret_retrieval(hostname, context, username, node_name):
    qpylib.log('Retrieving Access Secret...')
    payload = json.dumps({'peerNodeName': node_name})
    raw_response = send_rest_request(hostname, 'AccessSecret', context, username, payload)
    response = json.loads(raw_response)
    qpylib.log('Received secret token' + '\n')
    return response['secret']


def perform_pxGrid_secret_retrieval(host, 
                                    port, 
                                    key_file, 
                                    cert_file, 
                                    cacerts, 
                                    user_name, 
                                    node_name):
    qpylib.log('Retrieving pxGrid AccessSecret')
    payload = json.dumps({'peerNodeName': node_name})
    raw_response = send_pxgrid_api_request(host, port, key_file, cert_file, cacerts, user_name, '/pxgrid/control/AccessSecret', payload)
    response = json.loads(raw_response)
    return response['secret']


def deal_with_client(connstream):
        qpylib.log('Reading data')
        data = connstream.recv()
        # data1 = connstream.read()
        # null data means the client is finished with us
        if data is not None:
            qpylib.log('Data from connection stream: ' + str(data)[0:5]+'.....')
        while data:
            if not do_something(connstream, data):
                # we'll assume do_something returns False
                # when we're finished with client
                break
            data = connstream.read()
        # finished with client


def do_something(connstream, data):
    return False


def test_ssl_http_connection(host, port, key_file, cert_file, cacerts, user_name, environment, msg):
    qpylib.log("{0} http connection with {1} server".format(msg, environment))
    test_status = 0
    qpylib.log('cacerts>>>>>>>>>>>>>'+str(cacerts))
    if environment == 'primary':
        cert_path = 'store/primary/'
    else:
        cert_path = 'store/secondary/'
    https_conn = None
    try:
        https_conn = httplib.HTTPSConnection(host, port)
        sock = socket.create_connection((host, port))
        qpylib.log("{0} connection with {1} server using PROTOCOL_SSLv23".format(msg, environment))
        https_conn.sock = ssl.wrap_socket(sock,
                                        keyfile=cert_path+key_file, 
                                        certfile=cert_path+cert_file, 
                                        server_side=False, 
                                        cert_reqs=ssl.CERT_REQUIRED, 
                                        ssl_version=ssl.PROTOCOL_SSLv23, 
                                        ca_certs=cert_path+cacerts,
                                        do_handshake_on_connect=True, 
                                        suppress_ragged_eofs=True)

        qpylib.log("SSL Socket created while {0}".format(msg))
        payload = json.dumps({'description': 'pXGrid App for Qradar', 'groups': 'Session'})
        b64 = base64.b64encode((user_name + ':' + '').encode()).decode()
        qpylib.log("Sending {0} request".format(msg))
        qpylib.log('user_name>>>>>>>>'+str(user_name))
        https_conn.request(method="POST",
            url = "/pxgrid/control/AccountActivate",
            headers = {"Content-type": "application/json", 'Connection': 'keep-alive','Authorization': 'Basic ' + b64},
            body = payload)
        res = https_conn.getresponse()
        qpylib.log("{0} response got from {1} server".format(msg, environment))

        if res and res.status:
            test_status = res.status
            qpylib.log("{0} status from {1} server: {2}".format(msg, environment, str(res.status)))

        body = res.read()
        if res and body:
            qpylib.log("{0} response from {1} server: {2}".format(msg, environment, body))

    except ssl.SSLError, e:
        test_status = 403
        qpylib.log("SSL Error while invoking {0} http connection {1}".format(environment, str(e)))
        qpylib.log(traceback.format_exc())
    except Exception, e:
        test_status = 401
        qpylib.log("Exception while {0} http connection with {1} server".format(msg, environment))
        qpylib.log(traceback.format_exc())

    finally:
        if https_conn:
            https_conn.close()

    return test_status


def test_ws_connection(host, port, key_file, cert_file, cacerts, user_name):
    qpylib.log("Trying ws connection ..................")
    test_status = 0

    dbc = DBClient()
    active_server = str(dbc.get_active_server_details()['switch_status'])
    cert_path = 'store/' + active_server + '/'
    https_conn = None
    try:
        https_conn = httplib.HTTPSConnection(host, port)
        sock = socket.create_connection((host, port))
        qpylib.log("Wrapping SSL socket ..................")
        https_conn.sock = ssl.wrap_socket(sock,
                                        keyfile=cert_path+key_file, 
                                        certfile=cert_path+cert_file, 
                                        server_side=False, 
                                        cert_reqs=ssl.CERT_REQUIRED, 
                                        ssl_version=ssl.PROTOCOL_SSLv23, 
                                        ca_certs=cert_path+cacerts,
                                        do_handshake_on_connect=True, 
                                        suppress_ragged_eofs=True)

        qpylib.log("SSL Socket created..................")
        payload = json.dumps({'description': 'pXGrid App for Qradar', 'groups': 'Session'})
        b64 = base64.b64encode((user_name + ':' + '').encode()).decode()
        qpylib.log("Sending test request ..................")
        https_conn.request(method="POST",
            url="/pxgrid/control/AccountActivate",
            headers={"Content-type": "application/json", 'Connection': 'keep-alive','Authorization': 'Basic ' + b64},
            body=payload)

        res = https_conn.getresponse()
        if res:
            test_status = res.status
        qpylib.log("Executed test_ws_connection for pxGrid ..................")
        if res.status is not None:
            qpylib.log("Response status from pxGrid .................."+str(res.status))
        body = res.read()
    except ssl.SSLError, e:
        test_status = 403
        qpylib.log("Using SSLv23.", str(e))
    except Exception, e:
        test_status = 401
        qpylib.log("Exception while invoking test_ws_connection connection")
        qpylib.log(traceback.format_exc())
    finally:
        if https_conn:
            https_conn.close()

    return test_status


def activatePxGridAccount(host, port, key_file, cert_file, cacerts, payload, user_name, secret, _url):
    activate_status = 0
    dbc = DBClient()
    active_server = str(dbc.get_active_server_details()['switch_status'])
    cert_path = 'store/' + active_server + '/'
    https_conn = None
    try:
        https_conn = httplib.HTTPSConnection(host, port)
        sock = socket.create_connection((host, port))

        https_conn.sock = ssl.wrap_socket(sock,
                                        keyfile=cert_path+key_file, 
                                        certfile=cert_path+cert_file, 
                                        server_side=False, 
                                        cert_reqs=ssl.CERT_REQUIRED, 
                                        ssl_version=ssl.PROTOCOL_SSLv23,
                                        ca_certs=cert_path+cacerts,
                                        do_handshake_on_connect=True, 
                                        suppress_ragged_eofs=True)

        b64 = base64.b64encode((user_name + ':' + secret).encode()).decode()
        https_conn.request(
            method="POST",
            url=_url,
            headers={"Content-type": "application/json", 'Connection': 'keep-alive', 'Authorization': 'Basic ' + b64},
            body=payload)
        res = https_conn.getresponse()
        activate_status = res.status
        body = res.read()
        qpylib.log("Account Activation details: {0}".format(str(body)))
    except ssl.SSLError, ssle:
        activate_status = ssle.errno
        qpylib.log("Trying SSLv23.", str(ssle))
    except socket.error, se:
        qpylib.log('Server may be down. Will retry with secondary server in next excecution.')
        activate_status = se.errno
    except Exception as ex:
        activate_status = 1
        qpylib.log("Exception while invoking activatePxGridAccount connection")
        qpylib.log(traceback.format_exc())
    finally:
        if https_conn:
            https_conn.close()
    return activate_status


def send_request_to_pxgrid_api(host, port, key_file, cert_file, cacerts, payload, username, secret, _url):
    base_url = '/pxgrid/control/'
    dbc = DBClient()
    active_server = str(dbc.get_active_server_details()['switch_status'])
    cert_folder = 'store/' + active_server + '/'
    body = None
    https_conn = None
    try:
        https_conn = httplib.HTTPSConnection(host, port)
        sock = socket.create_connection((host, port))
        https_conn.sock = ssl.wrap_socket(sock,
                                        keyfile=cert_folder+key_file, 
                                        certfile=cert_folder+cert_file, 
                                        server_side=False, 
                                        cert_reqs=ssl.CERT_REQUIRED, 
                                        ssl_version=ssl.PROTOCOL_SSLv23, 
                                        ca_certs=cert_folder+cacerts,
                                        do_handshake_on_connect=True, 
                                        suppress_ragged_eofs=True)
        b64 = base64.b64encode((username + ':' + secret).encode()).decode()
        https_conn.request(method="POST",
            url=base_url+_url,
            headers={"Content-type": "application/json", 'Connection': 'keep-alive','Authorization': 'Basic ' + b64},
            body=payload)
        res = https_conn.getresponse()
        qpylib.log("Response status from pxGrid .................."+str(res.status))
        body = res.read()
    except ssl.SSLError, e:
        qpylib.log("send_request_to_pxgrid_api -> Trying with SSLv23.", str(e))
    except Exception, e:
        qpylib.log("Exception while sending to request to pxGrid API", str(e))
        qpylib.log(traceback.format_exc())
    finally:
        if https_conn:
            https_conn.close()

    return body


def send_pxgrid_ws_request(ws_url,
                           topic_list,
                           client_cert_key_filepath,
                           client_cert_pem_filepath,
                           ca_cert_filepath,
                           username,
                           access_secret):
    global status
    status = 'stopped'
    qpylib.log('wsUrl: ' + ws_url)
    dbc = DBClient()
    active_server = str(dbc.get_active_server_details()['switch_status'])
    cert_folder = 'store/' + active_server+'/'

    b64 = base64.b64encode((username + ':' + access_secret).encode()).decode()
    _headers = [('Authorization', 'Basic ' + b64)]

    qpylib.log('Creating WebSocketClient....')
    wsc1 = WebSocketClient(ws_url, headers=_headers)
    ssl_sock = ssl.wrap_socket(wsc1.sock,
                               keyfile=cert_folder+client_cert_key_filepath,
                               certfile=cert_folder+client_cert_pem_filepath,
                               server_side=False,
                               cert_reqs=ssl.CERT_REQUIRED,
                               ssl_version=ssl.PROTOCOL_SSLv23,
                               ca_certs=cert_folder+ca_cert_filepath,
                               do_handshake_on_connect=True,
                               suppress_ragged_eofs=True)
    wsc1.sock = ssl_sock
    ws = None
    try:
        ws = EventCollector(ws_url, headers=_headers, topic_list=topic_list)
        qpylib.log('Connecting to websocket {0} on port {1}'.format(str(ws.host), str(ws.port)))
        ws.connect()
        qpylib.log('Connected and about to run for ever....')
        status = 'started'
        dbc.updatejob_status({'current_job_status': 'started'})
        ws.run_forever()
        qpylib.log('Websocket lost connection will retry....')
    except KeyboardInterrupt as kie:
        qpylib.log('KeyboardInterrupt closing connection')
        dbc.updatejob_status({'current_job_status': 'stopped'})
        status = 'stopped'
        raise kie
    except Exception as exp:
        qpylib.log(traceback.format_exc())
        qpylib.log('Exception closing connection')
        qpylib.log('Type: ' + str(type(exp)))
        qpylib.log('Exception: ' + str(exp))
        dbc.updatejob_status({'current_job_status': 'stopped'})
        status = 'stopped'
        qpylib.log('Message subscription status: {0}'.format(status))
        raise exp
    return status


class EventCollector(WebSocketClient):

    def __init__(self, ws_url, headers=None, topic_list=[]):
        self.topic_list = topic_list
        WebSocketClient.__init__(self, ws_url, headers=headers)

    def opened(self):
        for t in self.topic_list:
            subscribe_frame = StompFrame()
            subscribe_frame.set_command('SUBSCRIBE')
            subscribe_frame.set_header('destination', t)
            subscribe_frame.set_header('id', 'QRadarAppForPxGrid')  # should be using a better id
            out_stream = io.StringIO()
            subscribe_frame.write(out_stream)
            data = out_stream.getvalue().encode()
            # data = "SUBSCRIBE\ndestination:/topic/com.cisco.ise.session\nid:px-id\n\n\x00".encode()
            self.send(data, True)
            qpylib.log("Subscribe request sent to websocket for {0}".format(t))

    # def closed(self, code, reason):
    #     qpylib.log("EventCollector is closed...")
    #     qpylib.log("EventCollector code : {0}".format(str(code)))
    #     qpylib.log("EventCollector reason: {0}".format(str(reason)))
    #     qpylib.log("Webclient session will be restarted soon - See pxGrid --> Administrations --> pxGrid Services --> Web Clients")
    #     dbc = DBClient()
    #     dbc.updatejob_status({'current_job_status':'stopped'})

    def closed(self, code, reason):
        qpylib.log("Websocket is closed.")
        qpylib.log("Websocket closing reason code: " + str(code))
        qpylib.log("Websocket closing reason: " + str(reason))

    def received_message(self, m):
        qpylib.log("EventCollector --> received_message invoked...")
        qradar_api_instance = qpylib.strategy()
        qradar_ip = qradar_api_instance.get_console_address()
        qpylib.log("QRadar Console Address: {0}".format(str(qradar_ip)))
        out_stream = io.StringIO()
        in_stream = None
        try:
            qpylib.log('m.data: {0}'.format(str(m.data)))
            in_stream = io.StringIO(m.data.decode())
            qpylib.log('m.data.decode: {0}'.format(str(m.data.decode())))
        except UnicodeDecodeError, ude:
            qpylib.log('UnicodeDecode Issue')
            qpylib.log('m.data Type: ' + str(type(m.data)))
            qpylib.log('UnicodeDecode Issue: ' + str(ude))
            import traceback
            qpylib.log(traceback.format_exc())
            try:
                in_stream = io.StringIO(m.data.decode('utf-8'))
            except UnicodeDecodeError as ude1:
                qpylib.log('UnicodeDecode Issue with utf-8')
                in_stream = io.StringIO(m.data.encode('utf-8').decode('utf-8'))
            except UnicodeEncodeError as uee:
                qpylib.log('UnicodeEncode Issue : ' + str(uee))
                try:
                    in_stream = io.StringIO(unicode(m.data,'utf-8').decode('utf-8'))
                except Exception as e1:
                    qpylib.log('UnicodeEncode Exception while converting to unicode')
                    import traceback
                    qpylib.log(traceback.format_exc())
            except Exception as e2:
                qpylib.log('Message type is : ' + str(type(m)))
                qpylib.log('Message data type is : ' + str(type(m.data)))
                qpylib.log(traceback.format_exc())
        except Exception as e:
            qpylib.log('message Type: ' + str(type(m)))
            qpylib.log('message data Type: ' + str(type(m.data)))
            qpylib.log('Exception: ' + str(e))
            import traceback
            qpylib.log(traceback.format_exc())

        message_frame = StompFrame()
        message_frame.parse(in_stream)
        #qpylib.log('Message content: ' + str(message_frame.content))
        response = None
        try:
            #qpylib.log('Message content type: ' + str(type(message_frame.content)))
            msg = str(message_frame.content)
            #qpylib.log('Msg type: ' + str(type(msg)))
            items = json.loads(str(msg)[:-1])
            #qpylib.log('Received notification: ' + str(message_frame.content))
        except Exception as e:
            import traceback
            qpylib.log(traceback.format_exc())

        if len(m) == 175:
            self.close(reason='bye bye')

        leef = CiscoLEEF_Logger(qradar_ip, 514, 'CISCO', 'QRadarAppForPxGrid', '1.0', facility=Facility.SYSLOG, delimiter="|")

        qpylib.log('Number of events received: ' + str(len(items)))
        _type = None
        for key in items:  # only one key here
            _type = key
            qpylib.log("Key: "+str(_type))

        if _type == 'sessions':
            session_event_str = leef.create_and_send_events('User Sessions', 'Sessions', _type, items, leef.LEEF_HEADER)
        elif _type == 'failures':
            radius_event_str = leef.create_and_send_events('Radius Failure', 'Radius', _type, items, leef.LEEF_HEADER)
        elif _type == 'status':
            status_event_str = leef.create_and_send_events('ANC Actions', 'Status', _type, items, leef.LEEF_HEADER)
        elif _type == 'endpoints':
            endpoints_event_str = leef.create_and_send_events('MDM', 'Endpoints', _type, items, leef.LEEF_HEADER)
        else:
            qpylib.log('Unknown/Non-subscribed event type:' + str(_type))


class StompFrame:
    __author__ = 'alei'

    def __init__(self):
        self.headers = {}
        self.content = None
        self.command = None

    def set_command(self, command):
        self.command = command

    def set_content(self, content):
        self.content = content

    def set_header(self, key, value):
        self.headers[key] = value

    def write(self, out):
        out.write(self.command.decode('utf-8'))
        out.write(u"\n")
        for key in self.headers:
            out.write(key.decode('utf-8'))
            out.write(u":")
            out.write(self.headers[key].decode('utf-8'))
            out.write(u"\n")
        out.write(u"\n")
        if self.content is not None:
            out.write(self.content)
        out.write(u"\0")

    def parse(self, in_stream):
        if in_stream is not None:
            self.command = in_stream.readline().rstrip("\r\n")
            # qpylib.log("Command: "+self.command)
            for line in in_stream:
                qpylib.log("Line: " + line)
                line = line.rstrip("\r\n")
                if line in "":
                    break
                if "Date:" in line:
                    continue
                (name, value) = line.split(":")
                self.headers[name] = value
            self.content = in_stream.read()

    def __repr__(self):
        from pprint import pformat
        return pformat(vars(self), indent=4)
